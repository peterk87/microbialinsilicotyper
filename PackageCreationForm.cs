﻿using System.Windows.Forms;
using BrightIdeasSoftware;

namespace MicrobialInSilicoTyper
{
    public partial class PackageCreationForm : Form
    {
        private readonly PackageTestCollection _package;

        public PackageCreationForm(PackageTestCollection package)
        {
            _package = package;
            InitializeComponent();

            SetupOLV();

            txtPackageName.KeyPress += (sender, args) => ValidatePackageNameInput(args);
            txtPackageName.LostFocus += (sender, args) => ValidatePackageNameInput(new KeyPressEventArgs((char)Keys.Enter));

            if (_package != null) txtPackageName.Text = _package.PackageDir.Name;


            btnRemove.Click += (sender, args) => OnRemoveTests();
            btnRemoveMI.Click += (sender, args) => OnRemoveTests();
            btnAdd.Click += (sender, args) => OnAddTest();
            btnAddMI.Click += (sender, args) => OnAddTest();
            btnEdit.Click += (sender, args) => OnEditTest();
            btnEditMI.Click += (sender, args) => OnEditTest();
        }

        private void SetupOLV()
        {
            olv.FullRowSelect = true;
            olv.UseTranslucentSelection = true;
            olv.OwnerDraw = true;
            olv.UseAlternatingBackColors = true;


            olv.Columns.Add(new OLVColumn
                                {
                                    Text = "Test Name",
                                    FillsFreeSpace = true,
                                    AspectGetter = o =>
                                                       {
                                                           var test = (PackageTest)o;
                                                           return test.TestName;
                                                       }
                                });

            olv.Columns.Add(new OLVColumn
                                {
                                    Text = "Test Type",
                                    FillsFreeSpace = true,
                                    TextAlign = HorizontalAlignment.Center,
                                    AspectGetter = o =>
                                                       {
                                                           var test = (PackageTest)o;
                                                           return test.TestType.ToString();
                                                       }
                                });
            olv.Columns.Add(new OLVColumn
                                {
                                    Text = "Marker",
                                    FillsFreeSpace = true,
                                    TextAlign = HorizontalAlignment.Center,
                                    AspectGetter = o =>
                                                       {
                                                           var test = (PackageTest)o;
                                                           return test.Markers.Count;
                                                       }
                                });

            olv.SetObjects(_package.Tests);

            olv.SelectionChanged += (sender, args) => btnEdit.Enabled = btnRemove.Enabled = (olv.SelectedObjects.Count > 0);

        }

        private void OnAddTest()
        {
            var chooseTestTypeForm = new TestTypeSelectionForm();
            if (chooseTestTypeForm.ShowDialog() != DialogResult.OK) return;

            var testType = chooseTestTypeForm.TestType1;

            chooseTestTypeForm.Dispose();

            var newTest = new PackageTest(_package.PackageDir);
            DialogResult result = DialogResult.Cancel;
            switch (testType)
            {
                case TestType.PCR:
                    var pcrTestForm = new CreatePCRTestForm(newTest);
                    result = pcrTestForm.ShowDialog();
                    pcrTestForm.Dispose();
                    break;
                case TestType.Allelic:
                    var alleleTestForm = new CreateAlleleTestForm(newTest);
                    result = alleleTestForm.ShowDialog();
                    alleleTestForm.Dispose();
                    break;
                case TestType.AmpliconProbe:
                case TestType.OligoProbe:
                    var probeTestForm = new CreateProbeTestForm(newTest);
                    result = probeTestForm.ShowDialog();
                    probeTestForm.Dispose();
                    break;
                case TestType.Repeat:
                    var repeatTestForm = new CreateRepeatTestForm(newTest);
                    result = repeatTestForm.ShowDialog();
                    repeatTestForm.Dispose();
                    break;
                case TestType.SNP:
                    var snpTestForm = new CreateSNPTestForm(newTest);
                    result = snpTestForm.ShowDialog();
                    snpTestForm.Dispose();
                    break;
            }
            if (result != DialogResult.OK)
                return;
            _package.Add(newTest);

            olv.SetObjects(_package.Tests);
        }

        private void OnRemoveTests()
        {
            if (olv.SelectedObjects.Count == 0)
                return;
            if (MessageBox.Show(string.Format("Are you sure you want to remove the selected test{0} from the current package?", (olv.SelectedObjects.Count > 1) ? "s" : ""),
                @"Microbial In Silico Typer",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) != DialogResult.Yes) return;
            foreach (var o in olv.SelectedObjects)
            {
                var test = (PackageTest)o;
                _package.Remove(test);
            }
            olv.SetObjects(_package.Tests);
        }

        private void OnEditTest()
        {
            if (olv.SelectedObjects.Count == 0) return;

            var test = (PackageTest)olv.SelectedObjects[0];
            switch (test.TestType)
            {
                case TestType.PCR:
                    var pcrTestForm = new CreatePCRTestForm(test);
                    pcrTestForm.ShowDialog();
                    pcrTestForm.Dispose();

                    break;
                case TestType.Allelic:
                    var alleleTestForm = new CreateAlleleTestForm(test);
                    alleleTestForm.ShowDialog();
                    alleleTestForm.Dispose();
                    break;
                case TestType.AmpliconProbe:
                case TestType.OligoProbe:
                    var probeTestForm = new CreateProbeTestForm(test);
                    probeTestForm.ShowDialog();
                    probeTestForm.Dispose();
                    break;
                case TestType.Repeat:
                    var repeatTestForm = new CreateRepeatTestForm(test);
                    repeatTestForm.ShowDialog();
                    repeatTestForm.Dispose();
                    break;
                case TestType.SNP:
                    var snpTestForm = new CreateSNPTestForm(test);
                    snpTestForm.ShowDialog();
                    snpTestForm.Dispose();
                    break;
            }


            olv.Refresh();
        }

        private void ValidatePackageNameInput(KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            //illegal characters in filenames/directory names
            //\ / : * ? " < > |
            switch (c)
            {
                case (char)Keys.Tab:
                case (char)Keys.Enter:
                    if (txtPackageName.Text.Length > 0)
                        _package.Name = txtPackageName.Text;
                    break;
                case '|':
                case '<':
                case '>':
                case '?':
                case '*':
                case ':':
                case '"':
                case '/':
                case '\\':
                    e.Handled = true;
                    break;
                default:
                    e.Handled = false;
                    break;
            }
        }
    }
}
