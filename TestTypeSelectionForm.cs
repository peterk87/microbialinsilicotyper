﻿using System.Drawing;
using System.Windows.Forms;

namespace MicrobialInSilicoTyper
{
    public partial class TestTypeSelectionForm : Form
    {
        private TestType _testType;

        public TestTypeSelectionForm()
        {
            _testType = TestType.PCR;
            InitializeComponent();

            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;

            //have the new test type selection window pop up underneath the user's cursor
            Location = new Point(MousePosition.X - Width/2, MousePosition.Y - Height/2);

            //setup radio button click events
            radAllelic.Click += (sender, args) => OnRadioButtonClick(radAllelic);
            radVNTR.Click += (sender, args) => OnRadioButtonClick(radVNTR);
            radPCR.Click += (sender, args) => OnRadioButtonClick(radPCR);
            radProbe.Click += (sender, args) => OnRadioButtonClick(radProbe);
            radSNP.Click += (sender, args) => OnRadioButtonClick(radSNP);
            //setup OK button click event; set the test type
            btnOK.Click += (sender, args) => OnOKClick();
        }

        public TestType TestType1
        {
            get { return _testType; }
        }

        private void OnOKClick()
        {
            if (radAllelic.Checked)
            {
                _testType = TestType.Allelic;
            }
            else if (radVNTR.Checked)
            {
                _testType = TestType.Repeat;
            }
            else if (radPCR.Checked)
            {
                _testType = TestType.PCR;
            }
            else if (radProbe.Checked)
            {
                _testType = TestType.OligoProbe;
            }
            else if (radSNP.Checked)
            {
                _testType = TestType.SNP;
            }
        }

        private void OnRadioButtonClick(RadioButton rad)
        {
            radAllelic.Checked = false;
            radVNTR.Checked = false;
            radPCR.Checked = false;
            radProbe.Checked = false;
            radSNP.Checked = false;

            rad.Checked = true;
        }
    }
}
