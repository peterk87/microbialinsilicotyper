﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace MicrobialInSilicoTyper
{
    public partial class MainForm : Form
    {

        /// <summary>Hash set of genome multifasta file paths.</summary>
        private readonly HashSet<string> _files = new HashSet<string>();

        /// <summary>Open forms.</summary>
        private readonly FormCollection _frms;
        
        /// <summary>List of marker selection OLVs.</summary>
        private readonly List<FastObjectListView> _olvMarkers = new List<FastObjectListView>();

        /// <summary>Dictionary with test name keys corresponding to lists of Marker.</summary>
        private Dictionary<string, List<Marker>> _testMarkerDict = new Dictionary<string, List<Marker>>();

        /// <summary>Dictionary for test name keys corresponding to OLVs with Marker.</summary>
        private readonly Dictionary<string, FastObjectListView> _testOlvDict =
            new Dictionary<string, FastObjectListView>();

        /// <summary>In silico typing analysis object.</summary>
        private InSilicoTyping _ist;

        /// <summary>Name of selected package.</summary>
        private string _selectedPackage;

        public MainForm()
        {
            InitializeComponent();
            
            Misc.GetDefaultThreadPoolWorkerThreads();
            Misc.TempDir = new DirectoryInfo(Path.GetTempPath());
            
            _frms = new FormCollection(this);

            //File drop handling
            DragDrop += (sender, args) => OnFileDrop(args);
            DragOver += (sender, args) => OnFileDragOverEnter(args);
            DragEnter += (sender, args) => OnFileDragOverEnter(args);
            
            //set event for running analysis with selected tests
            btnRun.Click += (sender, args) => RunAnalysisWithSelectedTests();
            //setup OLVs
            SetupPackagesListview();
            SetupTestsListview();
            SetupFastaOlv();
            
            AllowDrop = true;

            Misc.BlastWordSize = 7;

            txtBlastWordSize.KeyPress += (sender, args) => OnBlastWordSizeKeyPress(args);
            txtBlastWordSize.LostFocus += (sender, args) => OnBlastWordSizeKeyPress(new KeyPressEventArgs((char) Keys.Enter));

            btnClose.Click += (o, args) => OnClose();
            btnExit.Click += (o, args) => Application.Exit();
            btnAddGenomes.Click += (o, args) => OnBrowseAddGenomes();
            resultsFormToolStripMenuItem.Click += (o, args) => ShowResultsForm();
            bgw.DoWork += (o, args) => OnRun();
            bgw.ProgressChanged += (o, args) => ShowResultsForm();
            bgw.RunWorkerCompleted += (o, args) => OnComplete();
            bgw.WorkerSupportsCancellation = true;
            btnCancel.Click += (sender, args) => OnCancel();
            
            txtCores.Text = Environment.ProcessorCount.ToString(CultureInfo.InvariantCulture);
            txtCores.KeyPress += (sender, args) => OnChangeCores(args);
            txtCores.LostFocus += (sender, args) => OnChangeCores(new KeyPressEventArgs('\r'));
            
        }

        private void OnChangeCores(KeyPressEventArgs args)
        {
            int i;
            if (int.TryParse(args.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || args.KeyChar == '\b')
            {
                args.Handled = false;
            }
            else if (args.KeyChar == '\t' || args.KeyChar == '\r')
            {
                if (int.TryParse(txtCores.Text, out i))
                {
                    if (i > 0)
                    {
                        Misc.Cores = i;
                    }
                    else
                    {
                        txtCores.Text = Environment.ProcessorCount.ToString(CultureInfo.InvariantCulture);
                        Misc.SetDefaultThreadPoolWorkerThreads();

                    }
                }
            }
            else
            {
                args.Handled = true;
            }

        }

        private void OnCancel()
        {
            bgw.CancelAsync();
            btnCancel.Enabled = false;
            btnCancel.Visible = false;
        }

        private void OnBlastWordSizeKeyPress(KeyPressEventArgs args)
        {
            int i;
            if (int.TryParse(args.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || args.KeyChar == '\b')
            {
                args.Handled = false;
            }
            else if (args.KeyChar == '\t' || args.KeyChar == '\r')
            {
                if (int.TryParse(txtBlastWordSize.Text, out i))
                {
                    Misc.BlastWordSize = i;
                }
            }
            else
            {
                args.Handled = true;
            }

        }

        /// <summary>Setup OLV containing the list of typing tests.</summary>
        private void SetupTestsListview()
        {
            olvTests.UseTranslucentSelection = true;
            olvTests.OwnerDraw = true;
            olvTests.OwnerDrawnHeader = true;
            olvTests.HideSelection = false;
            olvTests.FullRowSelect = true;
            olvTests.UseAlternatingBackColors = true;


            olvTests.Columns.Add(new OLVColumn("Name", "Key")
                                     {
                                         FillsFreeSpace = true,
                                     });

            olvTests.Columns.Add(new OLVColumn
                                     {
                                         Text = "Type",
                                         AspectGetter = o =>
                                                            {
                                                                var pair = (KeyValuePair<string, List<Marker>>)o;
                                                                List<Marker> markers = pair.Value;
                                                                return markers[0].TypingTest;
                                                            }
                                     });

            //olvTests.Columns.Add(new OLVColumn
            //                         {
            //                             Text = "Selected",
            //                             AspectGetter = o =>
            //                                                {
            //                                                    var pair = (KeyValuePair<string, List<Marker>>)o;
            //                                                    FastObjectListView olv = _testOlvDict[pair.Key];
            //                                                    return olv.SelectedObjects.Count;
            //                                                }
            //                         });

            olvTests.Columns.Add(new OLVColumn
                                     {
                                         Text = "Count",
                                         AspectGetter = o =>
                                                            {
                                                                var pair = (KeyValuePair<string, List<Marker>>)o;
                                                                return pair.Value.Count;
                                                            }
                                     });


            olvTests.SelectionChanged += (sender, args) => UpdateFastaListview();
        }

        /// <summary>Setup OLV containing the list of packages.</summary>
        private void SetupPackagesListview()
        {
            olvPackages.UseTranslucentSelection = true;
            olvPackages.MultiSelect = false;
            olvPackages.FullRowSelect = true;
            olvPackages.OwnerDraw = true;
            olvPackages.UseAlternatingBackColors = true;
            olvPackages.Columns.Add(new OLVColumn("Name", "Name") { FillsFreeSpace = true });

            olvPackages.SelectionChanged += (sender, args) => OnPackageSelectionChanged();

            btnEditPackage.Click += (sender, args) => EditPackage();

            btnNewPackage.Click += (sender, args) => CreatePackage();
            btnRemovePackage.Click += (sender, args) => RemovePackage();

            UpdatePackagesOlv();
        }

        /// <summary>Prompt user if the selected package should be removed.</summary>
        private void RemovePackage()
        {
            //at least 1 package should be selected.
            if (olvPackages.SelectedObjects.Count <= 0)
                return;
            //prompt the user if the package should be removed.
            var selected = (DirectoryInfo)olvPackages.SelectedObjects[0];
            if (
                MessageBox.Show(string.Format("Are you sure you want to remove the {0} package?", selected.Name),
                                @"Remove package?",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) !=
                DialogResult.Yes) return;
            //if the user clicks Yes on the Message Box then remove the package
            selected.Delete(true);
            UpdatePackagesOlv();
        }

        /// <summary>Allow the user to create a new package.</summary>
        private void CreatePackage()
        {
            var tests = new PackageTestCollection();
            var f = new PackageCreationForm(tests);
            if (f.ShowDialog() == DialogResult.OK)
            {
                //if there were no tests added to the package then delete the package
                //inform the user that packages need tests
                if (tests.Tests.Count == 0)
                {
                    tests.PackageDir.Delete(true);
                    MessageBox.Show("A organism analysis packages must have typing tests.");
                }
                else
                {
                    OnPackageSelectionChanged();
                }
            }
            else
            {
                //remove package directory if user presses Cancel
                tests.PackageDir.Delete(true);
            }
            f.Dispose();
            OnPackageSelectionChanged();
            UpdatePackagesOlv();
        }

        /// <summary>Allow the user to edit an existing package.</summary>
        private void EditPackage()
        {
            if (olvPackages.SelectedObjects.Count <= 0)
                return;
            var selected = (DirectoryInfo)olvPackages.SelectedObjects[0];
            var tests = new PackageTestCollection(selected);
            var f = new PackageCreationForm(tests);
            if (f.ShowDialog() == DialogResult.OK)
            {
                //do something if OK is pressed
                OnPackageSelectionChanged();
            }
            f.Dispose();
        }

        /// <summary>When the user selects a package update the other OLVs to show the relevant information for the package.</summary>
        private void OnPackageSelectionChanged()
        {
            //if no package is selected then don't show any info in the other OLVs
            //mimic a Close button click event
            if (olvPackages.SelectedObjects.Count == 0)
            {
                OnClose();
                btnEditPackage.Enabled = false;
                btnRemovePackage.Enabled = false;
            }
            else
            {
                //Allow file dropping into the main form only when a package is selected
                btnEditPackage.Enabled = true;
                btnRemovePackage.Enabled = true;
                //get the selected package name
                var dir = (DirectoryInfo)olvPackages.SelectedObjects[0];
                _selectedPackage = dir.Name;
                //get the Marker files
                FileInfo[] fi = dir.GetFiles("*.markers", SearchOption.AllDirectories);
                //init a new in silico typing analysis object
                _ist = new InSilicoTyping(fi);
                //setup the marker OLVs for each test in the package
                //get the relevant extra test info for the package
                GetExtraTestInfo();
                //change the Main form title text
                Text = string.Format("MIST - {0}", _selectedPackage);
                UpdateTestsListview();
                olvTests.SelectAll();
            }
        }

        private void UpdateTestsListview()
        {
            _testMarkerDict = _ist.Markers.GroupBy(m => m.TestName).ToDictionary(g => g.Key, g => g.ToList());
            olvTests.SetObjects(_testMarkerDict);
        }

        private void UpdatePackagesOlv()
        {
            DirectoryInfo mainDir = (new FileInfo(Application.ExecutablePath)).Directory;
            if (mainDir == null) 
                return;
            DirectoryInfo[] matchDirs = mainDir.GetDirectories("packages");
            if (matchDirs.Length <= 0)
                return;
            DirectoryInfo packagesDir = matchDirs[0];
            olvPackages.SetObjects(packagesDir.GetDirectories());
            if (olvPackages.SelectedObjects.Count != 0) return;
            if (olvPackages.Items.Count > 0)
                olvPackages.Items[0].Selected = true;
        }

        private void SetupFastaOlv()
        {
            olvFasta.UseTranslucentSelection = true;
            olvFasta.MultiSelect = false;
            olvFasta.FullRowSelect = true;
            olvFasta.OwnerDraw = true;
            olvFasta.UseAlternatingBackColors = true;
        }
        
        private void OnRun()
        {
            if (_files.Count <= 0) return;
            _ist.AddGenomes(_files);
            _ist.Run(bgw);
        }

        private void OnComplete()
        {
            pbr.Visible = false;
            btnCancel.Visible = false;
            UpdateFastaListview();
            EnableControls(true);
            if (bgw.CancellationPending)
            {
                Debug.WriteLine("Backgroundworker cancellation pending...");
            }
            ShowResultsForm();
        }

        private void OnClose()
        {
            _frms.CloseAllWindows();
            _ist = null;
            olvFasta.SetObjects(null);
            olvTests.SetObjects(null);
            if (_files != null)
                _files.Clear();
            btnRun.Enabled = false;

            foreach (FastObjectListView olv in _olvMarkers)
            {
                olv.Dispose();
            }

            _olvMarkers.Clear();
            _testOlvDict.Clear();

            gbxPackages.Enabled = true;

            UpdatePackagesOlv();
        }

        private void OnBrowseAddGenomes()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Filter = @"FASTA (*.fasta;*.fna;*.ffn)|*.fasta;*.fna;*.ffn|All Files (*.*)|*.*";

                ofd.Multiselect = true;
                if (ofd.ShowDialog() != DialogResult.OK) return;

                gbxPackages.Enabled = false;

                //_files = new List<FileInfo>();
                foreach (string filename in ofd.FileNames)
                {
                    _files.Add(filename);
                }

                UpdateFastaListview();
                if (_files.Count > 0)
                {
                    btnRun.Enabled = true;
                }
            }
        }

        private void RunAnalysisWithSelectedTests()
        {
            AddExistingMultifastaToFilesList();
            UpdateFastaListview();
            SetMarkersForAnalysis();

            btnCancel.Visible = true;
            btnCancel.Enabled = true;
            EnableControls(false);
            bgw.RunWorkerAsync();
        }

        private void SetMarkersForAnalysis()
        {
            //by default set all Marker to unselected
            foreach (Marker marker in _ist.Markers)
            {
                marker.Selected = false;
            }
            //get user selected Marker
            foreach (object objPair in olvTests.SelectedObjects)
            {
                var pair = (KeyValuePair<string, List<Marker>>) objPair;
                List<Marker> markers = pair.Value;

                foreach (Marker marker in markers)
                {
                    marker.Selected = true;
                }
            }
        }

        private void AddExistingMultifastaToFilesList()
        {
            if (_ist.MultifastaFileDict == null)
                return;
            _files.UnionWith(_ist.MultifastaFileDict.Keys);
        }

        private void EnableControls(bool enableControls)
        {
            foreach (FastObjectListView olv in _olvMarkers)
            {
                olv.Enabled = enableControls;
            }

            btnRun.Enabled =
                btnAddGenomes.Enabled =
                btnClose.Enabled =
                resultsFormToolStripMenuItem.Enabled =
                enableControls;
        }

        private void GetExtraTestInfo()
        {
            var testMarkers = _ist.Markers.GroupBy(m => m.TestName).ToDictionary(g => g.Key, g => g.ToList());

            var filenameFileInfoDict = ((DirectoryInfo) olvPackages.SelectedObjects[0])
                .GetFiles("*", SearchOption.AllDirectories)
                .ToDictionary(f => f.Name, f => f);
            
            var extraInfo = (from p in testMarkers 
                             let testName = p.Key
                             let markers = p.Value
                             let filename = testName + ".txt" 
                             where filenameFileInfoDict.ContainsKey(filename) 
                             let fileInfo = filenameFileInfoDict[filename] 
                             select new ExtraTestInfo(markers, fileInfo, testName) into extraTestInfo 
                             where extraTestInfo.Read() 
                             select extraTestInfo)
                             .ToList();
            foreach (ExtraTestInfo extra in extraInfo)
            {
                _ist.AddExtraInfo(extra);
            }
        }

        private void ShowResultsForm()
        {
            if (_ist.MultifastaFileDict.Count <= 0) 
                return;

            if (_frms.ResultsFormWindow == null)
            {
                _frms.ResultsFormWindow = new ResultsForm(_ist, _frms);
                _frms.ResultsFormWindow.Show();
            }
            else
            {
                _frms.ResultsFormWindow.UpdateResultsForm();
            }
            _frms.ResultsFormWindow.Focus();
        }

        private void UpdateFastaListview()
        {
            olvFasta.Columns.Clear();
            olvFasta.Columns.Add(new OLVColumn
                                     {
                                         Text = "Genome",
                                         AspectGetter = o => ((KeyValuePair<string, double>) o).Key,
                                     });
            olvFasta.Columns.Add(new OLVColumn
                                     {
                                         Text = "Mbp",
                                         AspectGetter = o => ((KeyValuePair<string, double>) o).Value,
                                         AspectToStringFormat = "{0:0.0}",
                                     });

            var d = _files.Select(f => new FileInfo(f)).ToDictionary(f => f.Name, f => f.Length/1e6d);
            olvFasta.SetObjects(d);
        }

        #region Handle file drop into MIST

        private void OnFileDrop(DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
                return;
            var filenames = (string[])e.Data.GetData(DataFormats.FileDrop);
            var r = new Regex(@"\.f", RegexOptions.Compiled);
            var files = new List<FileInfo>();
            if (filenames.Length == 1)
            {
                var f = new FileInfo(filenames[0]);
                if (r.IsMatch(f.Extension))
                {
                    files.Add(f);
                }
                else switch (f.Extension)
                {
                    case "":
                        var dir = new DirectoryInfo(f.FullName);
                        FileInfo[] fastaFiles = dir.GetFiles("*.f*");
                        files.AddRange(fastaFiles);
                        break;
                    case ".fas":
                    case ".fasta":
                    case ".fna":
                    case ".ffn":
                        files.Add(f);
                        break;
                }
            }
            else
            {
                _files.UnionWith(filenames
                    .Select(filename => new FileInfo(filename))
                    .Where(f => r.IsMatch(f.Extension))
                    .Select(f => f.FullName));

            }

            UpdateFastaListview();

            if (_files.Count <= 0) 
                return;

            btnRun.Enabled = true;
            gbxPackages.Enabled = false;
        }

        private void OnFileDragOverEnter(DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
                return;
            var files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length > 1)
            {
                e.Effect = olvPackages.SelectedObjects.Count == 0 ? DragDropEffects.None : DragDropEffects.Copy;
            }
            else if (files.Length == 1)
            {
                var fileInfo = new FileInfo(files[0]);
                switch (fileInfo.Extension)
                {
                    case ".fasta":
                    case ".fna":
                    case ".ffn":
                    case "":
                        e.Effect = olvPackages.SelectedObjects.Count == 0 ? DragDropEffects.None : DragDropEffects.Copy;
                        break;
                    case ".bin":
                    case ".zip":
                        e.Effect = DragDropEffects.Copy;
                        break;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        #endregion

    }
}