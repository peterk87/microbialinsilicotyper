﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace MicrobialInSilicoTyper
{
    public partial class CreatePCRTestForm : Form
    {
        private readonly PackageTest _test;

        readonly List<PCRMarker> _markers = new List<PCRMarker>();

        private string _testName;

        public CreatePCRTestForm(PackageTest test)
        {
            _test = test;
            InitializeComponent();

            txtExtraInfo.TextChanged += (sender, args) => txtExtraInfo.Select(txtExtraInfo.Text.Length, 0);
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;

            SetupOLV();

            //check if new test
            if (_test == null || _test.Markers.Count == 0)
            {
                _testName = txtTestName.Text;
            }
            else
            {//setup existing test - user is changing something
                InitTestMarkers();
                txtTestName.Text = _test.TestName;
                _testName = _test.TestName;
                txtExtraInfo.Text = Misc.GetExtraTestInfoFilePath(_testName);
            }

            txtTestName.KeyPress += (sender, args) => OnTestNameKeyPress(args);
            txtTestName.LostFocus += (sender, args) => OnTestNameKeyPress(new KeyPressEventArgs((char)Keys.Enter));
            txtTestName.TextChanged += (sender, args) => btnOK.Enabled = txtTestName.Text != "" && _markers.Count > 0;


            txtExtraInfo.AllowDrop = true;
            txtExtraInfo.DragEnter += (sender, args) => Misc.OnDragOverOrEnter(args);
            txtExtraInfo.DragOver += (sender, args) => Misc.OnDragOverOrEnter(args);
            //don't allow the user to change the text within txtExtraInfo by keyboard presses
            txtExtraInfo.KeyPress += (sender, args) => args.Handled = true;
            txtExtraInfo.DragDrop += (sender, args) =>
            {
                if (!args.Data.GetDataPresent(DataFormats.FileDrop)) 
                    return;
                var files = (string[])args.Data.GetData(DataFormats.FileDrop);
                if (files.Length != 1) 
                    return;
                var filename = files[0];
                txtExtraInfo.Text = filename;
            };
            btnExtraInfo.Click += (sender, args) =>
            {
                var sfd = new OpenFileDialog
                              {
                                  Multiselect = false, 
                                  Title = "Select extra information file"
                              };
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    txtExtraInfo.Text = sfd.FileName;
                }
            };


            btnOK.Enabled = txtTestName.Text != "" && _markers.Count > 0;
            btnOK.Click += (sender, args) => OnOKClick();
        }

        private void OnOKClick()
        {
            _test.Clear();
            foreach (IMarker bm in _markers)
            {
                _test.Add(bm.GetMarker());
            }
            _test.WritePrimerFile(Path.Combine(_test.PackageDir.FullName, string.Format("{0}.markers", _test.TestName)));
            if (txtExtraInfo.Text == "")
                return;
            var extraInfoFile = new FileInfo(txtExtraInfo.Text);
            if (!extraInfoFile.Exists)
                return;
            string newFilePath = Path.Combine(_test.PackageDir.FullName, string.Format("{0}.txt", txtTestName.Text));
            if (newFilePath == extraInfoFile.FullName)
                return;
            extraInfoFile.CopyTo(newFilePath, true);

        }

        private void OnTestNameKeyPress(KeyPressEventArgs args)
        {
            if (args.KeyChar == (char)Keys.Enter || args.KeyChar == '\t')
            {
                _testName = txtTestName.Text;

                foreach (var pcrMarker in _markers)
                {
                    pcrMarker.TestName = _testName;
                }
                olv.Refresh();
            }
            else
            {
                args.Handled = false;
            }
        }

        private void SetupOLV()
        {
            olv.CellEditActivation = ObjectListView.CellEditActivateMode.DoubleClick;
            olv.FullRowSelect = true;
            olv.UseTranslucentSelection = true;
            olv.OwnerDraw = true;
            olv.UseAlternatingBackColors = true;
            olv.AllowDrop = true;

            olv.Columns.Add(new OLVColumn("Name", "Name") { FillsFreeSpace = true });
            olv.Columns.Add(new OLVColumn("Test Name", "TestName") { IsEditable = false, FillsFreeSpace = true });
            olv.Columns.Add(new OLVColumn("Forward Primer", "ForwardPrimer"));
            olv.Columns.Add(new OLVColumn("Reverse Primer", "ReversePrimer"));
            olv.Columns.Add(new OLVColumn("Amplicon Size", "AmpliconSize") { FillsFreeSpace = true });
            olv.Columns.Add(new OLVColumn("Amplicon Range Factor", "AmpliconRange") { FillsFreeSpace = true });
            olv.Columns.Add(new OLVColumn
                                {
                                    Text = "Amplicon Size Range",
                                    AspectGetter = o =>
                                                       {
                                                           var pcrMarker = (PCRMarker)o;
                                                           var ampSize = pcrMarker.AmpliconSize;
                                                           var ampRange = pcrMarker.AmpliconRange;
                                                           var lower = (int)Math.Floor(ampSize * (1d - ampRange));
                                                           var upper = (int)Math.Ceiling(ampSize * (1d + ampRange));
                                                           return string.Format("{0}-{1}", upper, lower);
                                                       },
                                    IsEditable = false,
                                    FillsFreeSpace = true
                                });


            olv.DragEnter += (s, e) => Misc.OnDragOverOrEnter(e);
            olv.DragDrop += (s, e) => OnPCRFileDrop(e);
            olv.KeyDown += (s, e) => OnKeyDownOLV(e);



            olv.SetObjects(_markers);
        }

        private void OnPCRFileDrop(DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var filenames = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string filename in filenames)
                {
                    GetPCRMarkerFromFile(filename);
                }
            }
            else if (e.Data.GetDataPresent(DataFormats.UnicodeText))
            {
                var text = (string)e.Data.GetData(DataFormats.UnicodeText);
                GetPCRMarkerFromText(text);
            }
        }
        
        private void OnKeyDownOLV(KeyEventArgs e)
        {
            //check if del has been pressed and if the user wants to remove some Marker from the test
            if (e.KeyCode == Keys.Delete)
            {
                if (olv.SelectedObjects.Count > 0)
                {
                    if (MessageBox.Show("Are you sure you want to remove the selected Marker from this test?",
                        "Remove selected Marker?",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    foreach (var o in olv.SelectedObjects)
                    {
                        var pcrMarker = (PCRMarker)o;
                        _markers.Remove(pcrMarker);
                    }
                    olv.SetObjects(_markers);
                }
            }

            if (e.Control && e.KeyCode == Keys.V)
            {
                //handle pasting of clipboard data into the OLV
                string text = Clipboard.GetText();
                if (string.IsNullOrEmpty(text))
                    return;
                GetPCRMarkerFromText(text);
                //update OLV to show new items
                olv.SetObjects(_markers);
            }

        }

        private void GetPCRMarkerFromFile(string filename)
        {
            using (var sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line == "")
                        continue;
                    var split = line.Split('\t');
                    if (split.Length != 5)
                        continue;
                    string name = split[0];
                    string forward = split[1];
                    string reverse = split[2];
                    int size;
                    if (!int.TryParse(split[3], out size))
                        continue;
                    double range;
                    if (!double.TryParse(split[4], NumberStyles.Any, CultureInfo.InvariantCulture, out range))
                        continue;
                    _markers.Add(new PCRMarker(_testName, name, forward, reverse, size, range));
                }
            }
            olv.SetObjects(_markers);
            olv.Refresh();
            btnOK.Enabled = txtTestName.Text != "" && _markers.Count > 0;
        }

        private void GetPCRMarkerFromText(string text)
        {
            using (var sr = new StringReader(text))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line == "")
                        continue;
                    var split = line.Split('\t');
                    if (split.Length != 5)
                        continue;
                    string name = split[0];
                    string forward = split[1];
                    string reverse = split[2];
                    int size;
                    if (!int.TryParse(split[3], out size))
                        continue;
                    double range;
                    if (!double.TryParse(split[4], NumberStyles.Any, CultureInfo.InvariantCulture, out range))
                        continue;
                    _markers.Add(new PCRMarker(_testName, name, forward, reverse, size, range));
                }
            }
            olv.SetObjects(_markers);
            olv.Refresh();
            btnOK.Enabled = txtTestName.Text != "" && _markers.Count > 0;
        }

        private void InitTestMarkers()
        {
            foreach (Marker m in _test.Markers)
            {
                if (m.TypingTest == TestType.PCR)
                {
                    _markers.Add(new PCRMarker(
                                     m.TestName,
                                     m.Name,
                                     m.ForwardPrimer,
                                     m.ReversePrimer,
                                     m.AmpliconSize,
                                     m.AmpliconRange));
                }
            }
            olv.SetObjects(_markers);
        }
    }
}
