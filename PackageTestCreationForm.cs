﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace MicrobialInSilicoTyper
{
    public partial class PackageTestCreationForm : Form
    {
        private readonly List<string> _allelePaths = new List<string>();
        private readonly List<IMarker> _markers = new List<IMarker>();

        private readonly PackageTest _test;
        private List<string> _alleleNames = new List<string>();

        private FileInfo _extraInfoDest;
        private FileInfo _extraInfoSource;

        public PackageTestCreationForm(PackageTest test)
        {
            _test = test;
            InitializeComponent();


            InitEvents();

            //if the user is changing an existing test then 
            if (_test == null || _test.Markers.Count <= 0)
                return;
            //setup the existing test
            txtTestName.Text = _test.TestName;
            InitTestMarkers();
            if (dgv.DataSource == null)
            {
                ChangeDataGridViewDataSource(_test.TestType);
            }
            CheckForExtraInfoFile();
        }

        private void InitEvents()
        {
            txtTestName.KeyPress += (sender, args) => ValidateTestNameInput(args);
            txtAlleleMarker.KeyPress += (sender, args) => ValidateAlleleMarkerNameInput(args);

            txtTestName.LostFocus += (sender, args) => ValidateTestNameInput(new KeyPressEventArgs((char) Keys.Enter));
            txtAlleleMarker.LostFocus += (sender, args) => ValidateAlleleMarkerNameInput(new KeyPressEventArgs((char) Keys.Enter));

            btnOK.Click += (sender, args) => OnOKClickCreateTest();

            tabControl1.Selected += (sender, args) => OnSelectedTabChanged(args);

            dgv.SelectionChanged += (sender, args) => btnRemove.Enabled = (dgv.SelectedRows.Count > 0);

            btnRemove.Click += (sender, args) => OnRemoveMarkers();
            btnExtraInfo.Click += (sender, args) => PromptSelectExtraInfoFile();

            btnAlleleBrowse.Click += (sender, args) => PromptSelectAlleleFiles();
            btnAlleleAddMarker.Click += (sender, args) => AddAlleleMarkers();

            txtAllelePath.KeyPress += (sender, args) => args.Handled = true;


            btnPCRBrowse.Click += (sender, args) => PromptSelectPCRFile();
            txtPCR.TextChanged += (sender, args) => ValidatePCRInfoTableInput();


            txtProbe.TextChanged += (sender, args) => ValidateProbeInput();

            btnProbeBrowse.Click += (sender, args) => PromptSelectProbeFile();
        }

        private void InitTestMarkers()
        {
            TestType t = TestType.PCR;

            foreach (Marker m in _test.Markers)
            {
                switch (m.TypingTest)
                {
                    case TestType.PCR:
                        _markers.Add(new PCRMarker(
                                         m.TestName,
                                         m.Name,
                                         m.ForwardPrimer,
                                         m.ReversePrimer,
                                         m.AmpliconSize,
                                         m.AmpliconRange));
                        break;
                    case TestType.Allelic:
                        _markers.Add(new AlleleMarker(
                                         m.TestName,
                                         m.Name,
                                         m.AllelicDatabaseFilename,
                                         _test.PackageDir
                                         ));
                        break;
                    case TestType.OligoProbe:
                        _markers.Add(new ProbeMarker(
                                         m.TestName,
                                         m.Name,
                                         m.ForwardPrimer,
                                         m.AmpliconRange
                                         ));
                        break;
                }
                t = m.TypingTest;
            }
            switch (t)
            {
                case TestType.PCR:
                    tabControl1.SelectTab(tabPCR);
                    break;
                case TestType.Allelic:
                    tabControl1.SelectTab(tabAllele);
                    break;
                case TestType.OligoProbe:
                    tabControl1.SelectTab(tabProbe);
                    break;
            }
        }

        private void CheckForExtraInfoFile()
        {
            DirectoryInfo dir = _test.PackageDir;
            FileInfo[] fi = dir.GetFiles(string.Format("{0}.txt", _test.TestName), SearchOption.AllDirectories);
            if (fi.Length <= 0)
                return;
            _extraInfoSource = fi[0];
            _extraInfoDest = _extraInfoSource;
            txtExtraInfo.Text = _extraInfoSource.FullName;
        }

        private static void ReadFileToTextbox(string filename, TextBox txt)
        {
            txt.Text = File.ReadAllText(filename);
        }

        private void OnOKClickCreateTest()
        {
            _test.Clear();
            foreach (IMarker bm in _markers)
            {
                _test.Add(bm.GetMarker());
            }
            _test.WritePrimerFile(Path.Combine(_test.PackageDir.FullName, string.Format("{0}.Marker", _test.TestName)));
            if (txtExtraInfo.Text == "") 
                return;
            var extraInfoFile = new FileInfo(txtExtraInfo.Text);
            if (!extraInfoFile.Exists) 
                return;
            string newFilePath = Path.Combine(_test.PackageDir.FullName, string.Format("{0}.txt", txtTestName.Text));
            extraInfoFile.CopyTo(newFilePath, true);
        }

        private void OnSelectedTabChanged(TabControlEventArgs e)
        {
            if (e.TabPage == tabPCR)
            {
                ChangeDataGridViewDataSource(TestType.PCR);
            }
            else if (e.TabPage == tabAllele)
            {
                ChangeDataGridViewDataSource(TestType.Allelic);
            }
            else if (e.TabPage == tabProbe)
            {
                ChangeDataGridViewDataSource(TestType.OligoProbe);
            }
        }

        private void ChangeDataGridViewDataSource(TestType t)
        {
            switch (t)
            {
                case TestType.PCR:
                    {
                        var tmp = new List<PCRMarker>();
                        foreach (IMarker marker in _markers)
                        {
                            tmp.Add((PCRMarker)marker);
                        }
                        if (tmp.Count > 0)
                            dgv.DataSource = tmp;
                    }
                    break;
                case TestType.Allelic:
                    {
                        var tmp = new List<AlleleMarker>();
                        foreach (IMarker marker in _markers)
                        {
                            tmp.Add((AlleleMarker)marker);
                        }
                        if (tmp.Count > 0)
                            dgv.DataSource = tmp;
                    }
                    break;
                case TestType.OligoProbe:
                    {
                        var tmp = new List<ProbeMarker>();
                        foreach (IMarker marker in _markers)
                        {
                            tmp.Add((ProbeMarker)marker);
                        }
                        if (tmp.Count > 0)
                            dgv.DataSource = tmp;
                    }
                    break;
            }
        }

        private void ValidateTestNameInput(KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            switch (c)
            {
                case '\t':
                case (char) Keys.Enter:
                    ChangeTestName();
                    break;
                case '|':
                case '<':
                case '>':
                case '?':
                case '*':
                case ':':
                case '"':
                case '/':
                case '\\':
                    e.Handled = false;
                    break;
                default:
                    e.Handled = true;
                    break;
            }
        }

        private void ChangeTestName()
        {
            string testName = txtTestName.Text;
            foreach (IMarker bm in _markers)
            {
                bm.TestName = testName;
            }
            if (_extraInfoDest != null && _extraInfoDest.Exists)
            {
                if (_extraInfoDest.Directory != null)
                {
                    string newExtraInfoFilePath = Path.Combine(_extraInfoDest.Directory.FullName, string.Format("{0}.txt", testName));
                    _extraInfoDest.MoveTo(newExtraInfoFilePath);
                    _extraInfoDest = new FileInfo(newExtraInfoFilePath);
                }
            }

            dgv.Refresh();
        }

        private void PromptSelectExtraInfoFile()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = "Select extra information file";
                ofd.Multiselect = false;
                if (ofd.ShowDialog() != DialogResult.OK) return;
                //get the extra information filepath
                txtExtraInfo.Text = ofd.FileName;
            }
        }

        private void OnRemoveMarkers()
        {
            if (MessageBox.Show(
                                "Are you sure you want to remove the selected marker(s) from the current test?",
                                "Remove marker(s)?",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) != DialogResult.Yes)
                return;
            var toRemove = new List<int>();
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].Selected)
                {
                    toRemove.Add(i);
                }
            }
            toRemove.Reverse();
            foreach (int i in toRemove)
            {
                _markers.RemoveAt(i);
            }

            ChangeDataGridViewDataSource(_markers[0].ResultType);
        }

        #region Allele

        private void ValidateAlleleMarkerNameInput(KeyPressEventArgs e)
        {
            char c = e.KeyChar;
            //illegal characters in filenames/directory names
            //\ / : * ? " < > |
            if (c == (char) Keys.Enter ||
                c == (char) Keys.Tab)
            {
                UpdateAlleleListView();
                btnAlleleAddMarker.Enabled = CheckAlleleMarkerValid();
            }
            else if (c != '\\' &&
                     c != '/' &&
                     c != '"' &&
                     c != ':' &&
                     c != '*' &&
                     c != '?' &&
                     c != '>' &&
                     c != '<' &&
                     c != '|')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void PromptSelectAlleleFiles()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = "Select allele multifasta file(s)";
                ofd.Multiselect = true;

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    foreach (string filename in ofd.FileNames)
                    {
                        _allelePaths.Add(filename);
                    }
                    txtAllelePath.Text = string.Join(";", _allelePaths);

                    if (txtAlleleMarker.Text == "" || _allelePaths.Count > 1)
                    {
                        _alleleNames = new List<string>();
                        foreach (string allelePath in _allelePaths)
                        {
                            var fi = new FileInfo(allelePath);
                            _alleleNames.Add(fi.Name.Remove(fi.Name.LastIndexOf('.')));
                        }
                        txtAlleleMarker.Text = string.Join(";", _alleleNames);
                    }
                    UpdateAlleleListView();
                }
            }
            btnAlleleAddMarker.Enabled = CheckAlleleMarkerValid();
        }

        private bool CheckAlleleMarkerValid()
        {
            if (txtAlleleMarker.Text == "")
                return false;

            foreach (string allelePath in _allelePaths)
            {
                if (!File.Exists(allelePath))
                    return false;
            }
            return true;
        }

        private void AddAlleleMarkers()
        {
            for (int i = 0; i < _allelePaths.Count; i++)
            {
                var marker = new AlleleMarker(
                    txtTestName.Text,
                    _alleleNames[i],
                    _allelePaths[i],
                    _test.PackageDir
                    );
                _markers.Add(marker);
            }

            var list = new List<AlleleMarker>();
            foreach (IMarker marker in _markers)
            {
                list.Add((AlleleMarker) marker);
            }

            dgv.DataSource = list;

            //clear info in the allele matching tab
            lvwAllele.Items.Clear();
            txtAlleleMarker.Text = "";
            txtAllelePath.Text = "";
            _allelePaths.Clear();
            _alleleNames.Clear();
            //disable "Add marker" button so that duplicates aren't added to the list of Marker for the test
            btnAlleleAddMarker.Enabled = false;
        }

        private void UpdateAlleleListView()
        {
            var lvl = new List<ListViewItem>();

            for (int i = 0; i < _allelePaths.Count; i++)
            {
                lvl.Add(new ListViewItem(new[]
                                             {
                                                 txtTestName.Text,
                                                 _alleleNames[i],
                                                 (new FileInfo(_allelePaths[i])).Name,
                                                 _allelePaths[i]
                                             }));
            }

            lvwAllele.Items.Clear();
            lvwAllele.Items.AddRange(lvl.ToArray());
        }

        #endregion

        #region PCR

        private void PromptSelectPCRFile()
        {
            using (var ofd = new OpenFileDialog {Multiselect = false})
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    ReadFileToTextbox(ofd.FileName, txtPCR);
                }
            }
        }

        private void ValidatePCRInfoTableInput()
        {
            var pcrMarkers = new List<PCRMarker>();
            string[] lines = txtPCR.Text.Split('\n');
            foreach (string line in lines)
            {
                string[] tmp = line.Split('\t');
                if (tmp.Length == 5)
                {
                    string name = tmp[0];
                    string forward = tmp[1];
                    string reverse = tmp[2];
                    int size;
                    if (int.TryParse(tmp[3], out size))
                    {
                        double range;
                        if (double.TryParse(tmp[4], NumberStyles.Any, CultureInfo.InvariantCulture, out range))
                        {
                            pcrMarkers.Add(new PCRMarker(txtTestName.Text, name, forward, reverse, size, range));
                        }
                    }
                }
            }

            _markers.Clear();
            _markers.AddRange(pcrMarkers);

            ChangeDataGridViewDataSource(TestType.PCR);

            dgv.Refresh();
        }

        #endregion

        #region Probe

        private void ValidateProbeInput()
        {
            var probeMarkers = new List<ProbeMarker>();
            double blastIdentity;
            if (double.TryParse(txtProbeIdentity.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out blastIdentity))
            {
                if (blastIdentity > 1)
                    blastIdentity = blastIdentity/100d;
                string[] lines = txtProbe.Text.Split('\n');
                var sb = new StringBuilder();
                string h = lines[0].Substring(1).Trim();
                var fastaEntries = new List<Tuple<string, string>>();
                for (int i = 1; i < lines.Length; i++)
                {
                    string tmp = lines[i].Trim();
                    if (tmp.Contains(">"))
                    {
                        fastaEntries.Add(new Tuple<string, string>(h, sb.ToString()));
                        h = tmp.Substring(1);
                        sb.Clear();
                    }
                    else
                    {
                        sb.Append(tmp);
                    }
                }
                fastaEntries.Add(new Tuple<string, string>(h, sb.ToString()));

                foreach (var fastaEntry in fastaEntries)
                {
                    probeMarkers.Add(new ProbeMarker(txtTestName.Text, fastaEntry.Item1, fastaEntry.Item2, blastIdentity));
                }

                _markers.Clear();
                _markers.AddRange(probeMarkers);

                ChangeDataGridViewDataSource(TestType.OligoProbe);

                dgv.Refresh();
            }
        }

        private void PromptSelectProbeFile()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Multiselect = false;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    ReadFileToTextbox(ofd.FileName, txtProbe);
                }
            }
        }
        
        #endregion
    }
}