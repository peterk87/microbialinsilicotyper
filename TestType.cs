namespace MicrobialInSilicoTyper
{
    public enum TestType
    {
        /// <summary>For PCR, binary absence or presence data is returned.</summary>
        PCR,
        /// <summary>For a sequence-typing based assay, an allele number designation is returned.</summary>
        Allelic,
        /// <summary>For a VNTR assay, the number of repeats is returned.</summary>
        Repeat,
        /// <summary>For an oligonucleotide (short) probe assay, binary absence or presence data is returned based on the full length %ID.</summary>
        OligoProbe,
        /// <summary>For a SNP-typing assay, the SNP designation is returned at the expected SNP position.</summary>
        SNP,
        /// <summary>For an amplicon-based (long) probe assay, binary absence or presence data is returned based on %ID and alignment length thresholds.</summary>
        AmpliconProbe
    }
}