﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace MicrobialInSilicoTyper
{
    public class PackageTestCollection
    {

        private readonly List<PackageTest> _tests = new List<PackageTest>();

        public List<PackageTest> Tests { get { return _tests; } }

        public string Name
        {
            get
            {
                return _pkgDir.Name;
            }
            set { if (_pkgDir.Parent != null) 
                _pkgDir.MoveTo(Path.Combine(_pkgDir.Parent.FullName, value)); 
            }
        }

        private DirectoryInfo _pkgDir;
        public DirectoryInfo PackageDir { get { return _pkgDir; } }

        public PackageTestCollection()
        {
            CreateDefaultDirectory();
        }

        public PackageTestCollection(DirectoryInfo pkgDir)
        {
            _pkgDir = pkgDir;
            FileInfo[] fi = _pkgDir.GetFiles("*.Markers", SearchOption.AllDirectories);
            if (fi.Length == 0)
                return;
            foreach (FileInfo filename in fi)
            {
                _tests.Add(new PackageTest(pkgDir, filename));
            }
        }

        private void CreateDefaultDirectory()
        {
            DirectoryInfo parentDir = (new FileInfo(Application.ExecutablePath)).Directory;
            if (parentDir != null)
            {
                DirectoryInfo[] subDirs = parentDir.GetDirectories("packages");
                if (subDirs.Length == 0)
                {
                    parentDir.CreateSubdirectory("packages");
                    subDirs = parentDir.GetDirectories("packages");
                }
                string newDirName = Path.Combine(subDirs[0].FullName, "MIST_package");
                string newDirName2 = newDirName;
                int counter = 1;
                while (Directory.Exists(newDirName2))
                {
                    newDirName2 = string.Format("{0} ({1})", newDirName, counter.ToString(CultureInfo.InvariantCulture));
                    counter++;
                }

                _pkgDir = subDirs[0].CreateSubdirectory(new DirectoryInfo(newDirName2).Name);
            }
            _pkgDir.CreateSubdirectory("alleles");

        }

        public void Add(PackageTest test)
        {
            _tests.Add(test);
        }

        public void Remove(PackageTest test)
        {
            //delete s file
            string markerFile = Path.Combine(_pkgDir.FullName, string.Format("{0}.Markers", test.TestName));
            if (File.Exists(markerFile))
            File.Delete(markerFile);

            string extraInfoFile = Path.Combine(_pkgDir.FullName, string.Format("{0}.txt", test.TestName));
            if (File.Exists(extraInfoFile))
            File.Delete(extraInfoFile);

            if (test.TestType == TestType.Allelic)
            {
                //find allele multifasta files and delete them
                foreach (Marker marker in test.Markers)
                {
                    string filename = marker.AllelicDatabaseFilename;
                    var fi = new FileInfo(filename);
                    var files = PackageDir.GetFiles(fi.Name, SearchOption.AllDirectories);
                    foreach (FileInfo fileInfo in files)
                    {
                        fileInfo.Delete();
                    }
                }
            }
            _tests.Remove(test);
        }
    }
}
