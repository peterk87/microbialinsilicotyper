﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BrightIdeasSoftware;
using System.Linq;

namespace MicrobialInSilicoTyper
{
    public partial class ResultsForm : Form
    {
        private readonly InSilicoTyping _analysis;
        private readonly Dictionary<OLVColumn, string> _columnsTests = new Dictionary<OLVColumn, string>();
        private readonly FormCollection _forms;
        
        private readonly Dictionary<string, Color> _testColor = new Dictionary<string, Color>();

        private FastObjectListView _olv;


        private TypingResultsCollection _typingResults;

        public ResultsForm(InSilicoTyping analysis, FormCollection forms)
        {
            _forms = forms;
            _analysis = analysis;
            
            InitializeComponent();
            SetupResultsForm();
            chkFuzzyMatches.CheckedChanged += (o, e) =>
                                                  {
                                                      _typingResults = new TypingResultsCollection(_analysis,
                                                                                                   chkFuzzyMatches.
                                                                                                       Checked);
                                                      _olv.SetObjects(null);
                                                      _olv.SetObjects(_typingResults.Results);
                                                  };

            btnShowMatchInfo.Click += (sender, args) => ShowMatchInfo();
            btnShowMatchInfo2.Click += (sender, args) => ShowMatchInfo();
            FormClosing += (sender, args) => _forms.ResultsFormWindow = null;
            contextMenuStrip1.Opening += (sender, args) => btnShowMatchInfo.Enabled = (_olv.SelectedObjects.Count > 0);

            btnSaveSparse.Click += (sender, args) => SaveTypingResults(false);
            btnSaveSparse2.Click += (sender, args) => SaveTypingResults(false);
            btnSaveVerbose.Click += (sender, args) => SaveTypingResults(true);
            btnSaveVerbose2.Click += (sender, args) => SaveTypingResults(true);

            btnResetResults.Click += (sender, args) => ResetTypingResultsToDefault();
            btnCalcPartitionCongruence.Click += (sender, args) =>
                                                    {
                                                        var form = new ComparePartitionsForm(_analysis);
                                                        form.Show();
                                                    };

            
            
        }

        /// <summary>Setup the OLV to show the typing results generated from in silico typing analysis.</summary>
        public void SetupResultsForm()
        {
            _olv = new FastObjectListView
            {
                Parent = toolStripContainer1.ContentPanel,
                Dock = DockStyle.Fill,
                IncludeColumnHeadersInCopy = true,
                UseTranslucentSelection = true,
                FullRowSelect = true,
                //GridLines = true,
                OwnerDraw = true,
                UseCellFormatEvents = true,
                //OwnerDrawnHeader = true,
                HeaderUsesThemes = false,
                ContextMenuStrip = contextMenuStrip1
            };


            _typingResults = new TypingResultsCollection(_analysis, chkFuzzyMatches.Checked);
            if (_typingResults.Results.Count == 0)
            {
                return;
            }

            _olv.Columns.Add(new OLVColumn
                                 {
                                     Text = "Sample",
                                     AspectGetter = o =>
                                                        {
                                                            var results = (TypingResults) o;
                                                            return results.ContigCollection.Name;
                                                        }
                                 });

            // Get a list of qualitative colours with an alpha value applied
            const int alphaValue = 128;
            int testCount = _typingResults.TestTestTypesDict.Count;
            var colorList = Misc.GetQualColourWithAlphaApplied(alphaValue, testCount);

            int count = 0;
            //setup columns for each test
            foreach (KeyValuePair<string, TestType> pair in _typingResults.TestTestTypesDict)
            {
                string testName = pair.Key;
                TestType testType = pair.Value;
                //add the new color for the test
                if (!_testColor.ContainsKey(testName))
                    _testColor.Add(testName, colorList[count++]);

                if (count == colorList.Count)
                    count = 0; //loop through the color list
                var headerStateStyle = new HeaderStateStyle
                                           {
                                               BackColor = _testColor[testName],
                                           };
                var headerStyle = new HeaderFormatStyle
                                      {
                                          Hot = headerStateStyle,
                                          Normal = headerStateStyle,
                                          Pressed = headerStateStyle
                                      };
                switch (testType)
                {
                    case TestType.PCR:
                    case TestType.OligoProbe:
                    case TestType.AmpliconProbe:
                        //for binary/probe tests show a binary absence/presence picture
                        var binaryProbeCol = new OLVColumn
                                                 {
                                                     Text = testName,
                                                     HeaderFormatStyle = headerStyle,
                                                     //render a bitmap of the absence presence data
                                                     RendererDelegate =
                                                         delegate(EventArgs e, Graphics g, Rectangle r, object o)
                                                             {
                                                                 g.FillRectangle(new SolidBrush(Color.White), r);
                                                                 g.DrawImage(
                                                                     ((TypingResults) o).GetBinaryAbsencePresenceBitmap(
                                                                         testName), r);
                                                                 return true;
                                                             },
                                                     AspectGetter = o =>
                                                                        {
                                                                            var results = (TypingResults) o;
                                                                            return results.GetBinaryString(testName);
                                                                        }
                                                 };
                        if (!_columnsTests.ContainsKey(binaryProbeCol))
                        {
                            _columnsTests.Add(binaryProbeCol, testName);
                            _olv.Columns.Add(binaryProbeCol);
                        }
                        break;

                    case TestType.Allelic:
                        foreach (Marker marker in _typingResults.TestMarkerDict[testName])
                        {
                            Marker marker1 = marker;
                            var alleleColumn = new OLVColumn
                                                   {
                                                       Text = marker.Name,
                                                       HeaderFormatStyle = headerStyle,
                                                       ToolTipText = marker.Name,
                                                       IsHeaderVertical = true,
                                                       Width = 40,
                                                       AspectGetter = o =>
                                                                          {
                                                                              var results = (TypingResults) o;
                                                                              return
                                                                                  results.GetMarkerResult(
                                                                                      testName, marker1,
                                                                                      chkFuzzyMatches.Checked);
                                                                          }
                                                   };
                            if (!_columnsTests.ContainsKey(alleleColumn))
                            {
                                _columnsTests.Add(alleleColumn, testName);
                                _olv.Columns.Add(alleleColumn);
                            }
                        }
                        break;
                    case TestType.Repeat:
                        foreach (Marker marker in _typingResults.TestMarkerDict[testName])
                        {
                            Marker marker1 = marker;
                            //Show the number of repeats
                            var repeatColumn = new OLVColumn
                                                   {
                                                       Text = marker.Name,
                                                       HeaderFormatStyle = headerStyle,
                                                       ToolTipText = marker.Name,
                                                       IsHeaderVertical = true,
                                                       Width = 40,
                                                       AspectGetter = o =>
                                                                          {
                                                                              var results = (TypingResults) o;
                                                                              return
                                                                                  results.GetMarkerResult(
                                                                                      testName, marker1,
                                                                                      chkFuzzyMatches.Checked);
                                                                          }
                                                   };
                            if (!_columnsTests.ContainsKey(repeatColumn))
                            {
                                _olv.Columns.Add(repeatColumn);
                                _columnsTests.Add(repeatColumn, testName);    
                            }
                        }
                        break;
                    case TestType.SNP:
                        foreach (Marker marker in _typingResults.TestMarkerDict[testName])
                        {
                            Marker marker1 = marker;
                            //show the SNP call
                            var repeatColumn = new OLVColumn
                                                   {
                                                       Text = marker.Name,
                                                       HeaderFormatStyle = headerStyle,
                                                       IsHeaderVertical = true,
                                                       Width = 23,
                                                       ToolTipText = marker.Name,
                                                       AspectGetter = o =>
                                                                          {
                                                                              var results = (TypingResults) o;
                                                                              return
                                                                                  results.GetMarkerResult(
                                                                                      testName, marker1,
                                                                                      chkFuzzyMatches.Checked);
                                                                          }
                                                   };
                            if (!_columnsTests.ContainsKey(repeatColumn))
                            {
                                _olv.Columns.Add(repeatColumn);
                                _columnsTests.Add(repeatColumn, testName);
                            }
                        }
                        break;
                }
                GetExtraTestInfoColumns(_columnsTests, headerStyle, testName);
            }
            //format each cell based on its respective test; special formatting for missing allele match info - show "No Match!"
            _olv.FormatCell += OnFormatResultsObjectListViewCell;

            _olv.SetObjects(null);
            _olv.SetObjects(_typingResults.Results);
            _olv.Show();
        }

        public void UpdateResultsForm()
        {
            _typingResults = new TypingResultsCollection(_analysis, chkFuzzyMatches.Checked);
            if (_typingResults.Results.Count == 0)
            {
                return;
            }
            if (_olv == null)
            {
                SetupResultsForm();
            }
            else
            {
                _olv.SetObjects(null);
                _olv.SetObjects(_typingResults.Results);
            }
        }

        private void OnFormatResultsObjectListViewCell(object sender, FormatCellEventArgs e)

        {
            string testName;
            if (!_columnsTests.TryGetValue(e.Column, out testName))
                return;
            Color newColor;
            if (!_testColor.TryGetValue(testName, out newColor))
                return;
            if (e.SubItem.Decorations.Count > 0)
            {
                e.SubItem.Decorations.Clear();
            }
            TestType testType;
            if (!_typingResults.TestTestTypesDict.TryGetValue(testName, out testType))
                return;
            if (testType == TestType.SNP)
            {
                switch (e.SubItem.Text)
                {
                    case "A":
                        e.SubItem.ForeColor = Color.White;
                        e.SubItem.BackColor = Color.ForestGreen;
                        break;
                    case "G":
                        e.SubItem.ForeColor = Color.White;
                        e.SubItem.BackColor = Color.Black;
                        break;
                    case "T":
                        e.SubItem.ForeColor = Color.White;
                        e.SubItem.BackColor = Color.Red;
                        break;
                    case "C":
                        e.SubItem.ForeColor = Color.White;
                        e.SubItem.BackColor = Color.Blue;
                        break;
                    default:
                        //color each cell based on the test it belongs to
                        var cellDec = new CellBorderDecoration
                                          {
                                              FillBrush = new SolidBrush(newColor),
                                              CornerRounding = 0f,
                                              BoundsPadding = new Size(0, 0),
                                          };
                        e.SubItem.Decorations.Add(cellDec);
                        break;
                }
                return;
            }

            //color each cell based on the test it belongs to
            var cbd = new CellBorderDecoration
                          {
                              FillBrush = new SolidBrush(newColor),
                              CornerRounding = 0f,
                              BoundsPadding = new Size(0, 0),
                          };
            e.SubItem.Decorations.Add(cbd);

            if (testType != TestType.Allelic)
            {
                return;
            }

            if (chkFuzzyMatches.Checked)
            {
                var typingResults = (TypingResults) e.Model;
                string markerName = e.Column.Text;

                List<MarkerMatch> markerMatches = typingResults.TestMatchDict[testName];
                MarkerMatch match = null;
                foreach (MarkerMatch markerMatch in markerMatches)
                {
                    if (markerMatch.Marker.Name == markerName)
                    {
                        match = markerMatch;
                        break;
                    }
                }
                if (match != null)
                    if (match.MarkerCall != e.SubItem.Text)
                    {
                        e.SubItem.Decorations.Add(new CellBorderDecoration
                                                      {
                                                          BorderPen = new Pen(Color.FromArgb(128, Color.Firebrick)),
                                                          FillBrush =
                                                              new SolidBrush(Color.FromArgb(128, Color.Firebrick)),
                                                          CornerRounding = 0f,
                                                          BoundsPadding = new Size(0, 0),
                                                      });
                    }
            }


            if (!string.IsNullOrEmpty(e.SubItem.Text))
                return;
            //if the test is allelic and the subitem text is empty then show "No Match!" in red with a red border 
            var noAlleleMatchDecoration = new TextDecoration("No Match!")
                                              {
                                                  Alignment = ContentAlignment.MiddleCenter,
                                                  Font = new Font(Font.Name, Font.SizeInPoints),
                                                  TextColor = Color.Firebrick,
                                              };
            e.SubItem.Decorations.Add(noAlleleMatchDecoration);
            var redCellBorder = new CellBorderDecoration
                                    {
                                        BorderPen = new Pen(Color.FromArgb(128, Color.Firebrick)),
                                        FillBrush = null,
                                        CornerRounding = 0f,
                                        BoundsPadding = new Size(0, 0),
                                    };
            e.SubItem.Decorations.Add(redCellBorder);
        }

        /// <summary>Generate columns for all of the extra info relating to the specified test.</summary>
        /// <param name="columnsTests">What test an OLV column corresponds to.</param>
        /// <param name="headerStyle">The header style for current test.</param>
        /// <param name="testName">Name of the current test.</param>
        private void GetExtraTestInfoColumns(Dictionary<OLVColumn, string> columnsTests, HeaderFormatStyle headerStyle,
                                             string testName)
        {
            if (_typingResults.TestExtraInfoDict[testName] == null)
                return;
            List<string> headers = _typingResults.TestExtraInfoDict[testName].GetExtraInfoHeaders();
            for (int i = 0; i < headers.Count; i++)
            {
                string header = headers[i];
                // extra info header index
                int i1 = i;
                var extraInfoCol = new OLVColumn
                                       {
                                           Text = header,
                                           HeaderFormatStyle = headerStyle,
                                           HeaderTextAlign = HorizontalAlignment.Center,
                                           TextAlign = HorizontalAlignment.Center,
                                           AspectGetter = o =>
                                                              {
                                                                  var results = (TypingResults) o;
                                                                  return results.GetExtraInfo(testName, i1);
                                                              }
                                       };
                columnsTests.Add(extraInfoCol, testName);
                _olv.Columns.Add(extraInfoCol);
            }
        }


        private void ShowMatchInfo()
        {
            if (_olv.SelectedObjects.Count > 0)
            {
                (new MatchInfoForm(_forms, _analysis, ((TypingResults)_olv.SelectedObjects[0]).ContigCollection, _typingResults)).Show();
            }
            else
            {
                TypingResults typingResults = null;
                foreach (var o in _olv.Objects)
                {
                    typingResults = (TypingResults)o;
                    break;
                }

                if (typingResults != null)
                    (new MatchInfoForm(_forms, _analysis, typingResults.ContigCollection, _typingResults)).Show();
            }
        }

        private void SaveTypingResults(bool verbose)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Title = string.Format("Save {0} in silico typing results", verbose ? "verbose" : "sparse");
                sfd.FileName = string.Format("MIST_results_{0}.txt", verbose ? "verbose" : "sparse");
                if (sfd.ShowDialog() != DialogResult.OK) return;
                _analysis.WriteResults(sfd.FileName, verbose);                
            }
        }

        private void ResetTypingResultsToDefault()
        {
            if (MessageBox.Show("Are you sure you want to reset the typing results back to default?",
                                "Reset typing results back to default?",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) != DialogResult.Yes) return;
            
            foreach (var cc in _analysis.MultifastaFileDict.Values)
            {
                foreach (MarkerMatch mm in cc.MarkerMatches)
                {
                    mm.DetermineIfCorrectMarkerMatch();
                }
            }
            //refresh listview to show updated marker match calls
            SetupResultsForm();
        }
    }
}