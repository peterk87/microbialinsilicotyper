using System;
using System.Collections.Generic;

namespace MicrobialInSilicoTyper
{
    public class ComparePartitions
    {
        private readonly string _methodA;
        private readonly string _methodB;

        private readonly int _partitionsA;
        private readonly int _partitionsB;

        private readonly Dictionary<string, Dictionary<string, int>> _contTable =
            new Dictionary<string, Dictionary<string, int>>();

        private readonly Dictionary<string, int> _sumCol = new Dictionary<string, int>();
        private readonly Dictionary<string, int> _sumRow = new Dictionary<string, int>();

        private readonly List<string> _typesA = new List<string>();
        private readonly List<string> _typesB = new List<string>();

        private readonly double _a;
        private readonly double _b;
        private readonly double _c;
        private readonly double _d;
        private readonly int _n;


        private readonly double _rand;

        private readonly double _adjustedRand;



        private readonly double _simpsonsA;
        private readonly double _simpsonsALower;
        private readonly double _simpsonsAUpper;

        private readonly double _simpsonsB;
        private readonly double _simpsonsBLower;
        private readonly double _simpsonsBUpper;


        private readonly double _wallaceIA;

        private readonly double _wallaceAvsB;
        private readonly double _wallaceAvsBLower;
        private readonly double _wallaceAvsBUpper;

        private readonly double _adjustedWallaceAvsB;
        private readonly double _adjustedWallaceAvsBLower;
        private readonly double _adjustedWallaceAvsBUpper;


        private readonly double _wallaceIB;

        private readonly double _wallaceBvsA;
        private readonly double _wallaceBvsALower;
        private readonly double _wallaceBvsAUpper;

        private readonly double _adjustedWallaceBvsA;
        private readonly double _adjustedWallaceBvsALower;
        private readonly double _adjustedWallaceBvsAUpper;


        public ComparePartitions(List<string> methodAValues, List<string> methodBValues, string methodA, string methodB)
        {
            _methodA = methodA;
            _methodB = methodB;
            _contTable = ContTable(methodAValues, methodBValues);
            GetContTableHeaders(_contTable, ref _typesA, ref _typesB);
            GetContTableTotals(_contTable, _typesA, _typesB, ref _sumCol, ref _sumRow, out _n);
            GetMisMatchMatrix(_contTable, _sumCol, _sumRow, _n, out _a, out _b, out _c, out _d);
            GetWallace(_a, _b, _c, out _wallaceAvsB, out _wallaceBvsA);
            _rand = GetRand(_a, _b, _c, _d);
            _adjustedRand = GetAdjustedRand(_a, _b, _c, _d, _n);
            GetSimpsons(methodAValues, out _simpsonsA, out _simpsonsALower, out _simpsonsAUpper, out _partitionsA);
            GetSimpsons(methodBValues, out _simpsonsB, out _simpsonsBLower, out _simpsonsBUpper, out _partitionsB);
            GetAdjustedWallace(_simpsonsA, _simpsonsB, _contTable, _sumCol, _sumRow, ref _wallaceAvsB, ref _wallaceBvsA,
                               out _wallaceAvsBLower, out _wallaceAvsBUpper, out _wallaceBvsALower,
                               out _wallaceBvsAUpper, out _wallaceIA, out _wallaceIB, out _adjustedWallaceAvsB,
                               out _adjustedWallaceAvsBLower, out _adjustedWallaceAvsBUpper, out _adjustedWallaceBvsA,
                               out _adjustedWallaceBvsALower, out _adjustedWallaceBvsAUpper);
        }

        public string MethodA
        {
            get { return _methodA; }
        }

        public string MethodB
        {
            get { return _methodB; }
        }

        public double WallaceAvsBLower
        {
            get { return _wallaceAvsBLower; }
        }

        public double WallaceAvsBUpper
        {
            get { return _wallaceAvsBUpper; }
        }

        public double WallaceBvsALower
        {
            get { return _wallaceBvsALower; }
        }

        public double WallaceBvsAUpper
        {
            get { return _wallaceBvsAUpper; }
        }

        public double SimpsonsB
        {
            get { return _simpsonsB; }
        }

        public int PartitionsB
        {
            get { return _partitionsB; }
        }

        public double SimpsonsBUpper
        {
            get { return _simpsonsBUpper; }
        }

        public double SimpsonsBLower
        {
            get { return _simpsonsBLower; }
        }

        public double AdjustedWallaceAvsB
        {
            get { return _adjustedWallaceAvsB; }
        }

        public double AdjustedWallaceAvsBUpper
        {
            get { return _adjustedWallaceAvsBUpper; }
        }

        public double AdjustedWallaceAvsBLower
        {
            get { return _adjustedWallaceAvsBLower; }
        }

        public double AdjustedWallaceBvsA
        {
            get { return _adjustedWallaceBvsA; }
        }

        public double AdjustedWallaceBvsAUpper
        {
            get { return _adjustedWallaceBvsAUpper; }
        }

        public double AdjustedWallaceBvsALower
        {
            get { return _adjustedWallaceBvsALower; }
        }

        public double WallaceIa
        {
            get { return _wallaceIA; }
        }

        public double WallaceIb
        {
            get { return _wallaceIB; }
        }


        public double SimpsonsA
        {
            get { return _simpsonsA; }
        }

        public double SimpsonsALower
        {
            get { return _simpsonsALower; }
        }

        public double SimpsonsAUpper
        {
            get { return _simpsonsAUpper; }
        }

        public int PartitionsA
        {
            get { return _partitionsA; }
        }


        public double A
        {
            get { return _a; }
        }

        public double B
        {
            get { return _b; }
        }

        public double C
        {
            get { return _c; }
        }

        public double D
        {
            get { return _d; }
        }

        public int N
        {
            get { return _n; }
        }

        public Dictionary<string, int> SumRow
        {
            get { return _sumRow; }
        }

        public Dictionary<string, int> SumCol
        {
            get { return _sumCol; }
        }

        public List<string> TypesB
        {
            get { return _typesB; }
        }

        public List<string> TypesA
        {
            get { return _typesA; }
        }

        public double WallaceAvsB
        {
            get { return _wallaceAvsB; }
        }

        public double WallaceBvsA
        {
            get { return _wallaceBvsA; }
        }

        public double Rand
        {
            get { return _rand; }
        }

        public double AdjustedRand
        {
            get { return _adjustedRand; }
        }

        private static void GetAdjustedWallace(
            double simpsonsA,
            double simpsonsB,
            Dictionary<string, Dictionary<string, int>> contTable,
            Dictionary<string, int> sumCol,
            Dictionary<string, int> sumRow,
            ref double wallaceAvsB,
            ref double wallaceBvsA,
            out double wallaceAvsBLower,
            out double wallaceAvsBUpper,
            out double wallaceBvsALower,
            out double wallaceBvsAUpper,
            out double wallaceIA,
            out double wallaceIB,
            out double adjustedWallaceAvsB,
            out double adjustedWallaceAvsBLower,
            out double adjustedWallaceAvsBUpper,
            out double adjustedWallaceBvsA,
            out double adjustedWallaceBvsALower,
            out double adjustedWallaceBvsAUpper)
        {
            if (Math.Abs(simpsonsA - 1.0) >= double.Epsilon && Math.Abs(simpsonsB - 1.0) >= double.Epsilon)
            {
                double rowSumWallaceAvsB = 0.0;
                double rowSumWallaceBvsA = 0.0;
                var colSumFc2 = new Dictionary<string, double>();
                var colSumFc3 = new Dictionary<string, double>();
                foreach (var pair1 in contTable)
                {
                    string i = pair1.Key;
                    var rSum = (double)sumCol[i];
                    double rowSumFc2 = 0.0;
                    double rowSumFc3 = 0.0;
                    foreach (var pair2 in pair1.Value)
                    {
                        string j = pair2.Key;
                        var val = (double)contTable[i][j];
                        int cSum = sumRow[j];
                        rowSumFc2 += Math.Pow(val / (rSum), 2.0);
                        rowSumFc3 += Math.Pow(val / (rSum), 3.0);

                        if (colSumFc2.ContainsKey(j))
                            colSumFc2[j] += Math.Pow(val / (cSum), 2.0);
                        else
                            colSumFc2[j] = Math.Pow(val / (cSum), 2.0);

                        if (colSumFc3.ContainsKey(j))
                            colSumFc3[j] += Math.Pow(val / (cSum), 3.0);
                        else
                            colSumFc3[j] = Math.Pow(val / (cSum), 3.0);
                    }
                    double rowVarSimpsons = 0.0;
                    if (rSum > 1.0)
                        rowVarSimpsons = GetVarSimpsons(rSum, rowSumFc2, rowSumFc3);
                    rowSumWallaceAvsB += Math.Pow((rSum * (rSum - 1.0)), 2.0) * rowVarSimpsons;
                    rowSumWallaceBvsA += rSum * (rSum - 1.0);
                }
                double colSumWallaceAvsB = 0.0;
                double colSumWallaceBvsA = 0.0;

                foreach (var pair in sumRow)
                {
                    string j = pair.Key;
                    int cSum = sumRow[j];
                    double fc2 = colSumFc2[j];
                    double fc3 = colSumFc3[j];
                    double colVarSimpsons = 0.0;
                    if (cSum > 1.0)
                        colVarSimpsons = GetVarSimpsons(cSum, fc2, fc3);
                    colSumWallaceAvsB += Math.Pow(cSum * (cSum - 1.0), 2.0) * colVarSimpsons;
                    colSumWallaceBvsA += cSum * (cSum - 1.0);
                }
                // get variance of Wallace 1vs2 (varW1) and 2vs1 (varW2);
                double varWallaceAvsB = 0.0;
                double varWallaceBvsA = 0.0;
                if (rowSumWallaceBvsA > 0.0)
                    varWallaceAvsB = (rowSumWallaceAvsB / Math.Pow((rowSumWallaceBvsA), 2.0));
                if (colSumWallaceBvsA > 0.0)
                    varWallaceBvsA = (colSumWallaceAvsB / Math.Pow((colSumWallaceBvsA), 2.0));
                // get mismatch matrix;

                wallaceIA = 1.0 - simpsonsA;
                wallaceIB = 1.0 - simpsonsB;

                adjustedWallaceAvsB = (wallaceAvsB - wallaceIB) / (1.0 - wallaceIB);
                adjustedWallaceBvsA = (wallaceBvsA - wallaceIA) / (1.0 - wallaceIA);

                double adjustedWallaceAvsBConfInterval = 2.0 * (1.0 / (1.0 - wallaceIB)) * Math.Sqrt(varWallaceAvsB);
                adjustedWallaceAvsBLower = adjustedWallaceAvsB - adjustedWallaceAvsBConfInterval;
                if (adjustedWallaceAvsBLower < 0.0)
                    adjustedWallaceAvsBLower = 0.0;
                adjustedWallaceAvsBUpper = adjustedWallaceAvsB + adjustedWallaceAvsBConfInterval;
                if (adjustedWallaceAvsBUpper > 1.0)
                    adjustedWallaceAvsBUpper = 0.0;

                double adjustedWallaceBvsAConfInterval = 2.0 * (1.0 / (1.0 - wallaceIA)) * Math.Sqrt(varWallaceBvsA);
                adjustedWallaceBvsALower = adjustedWallaceBvsA - adjustedWallaceBvsAConfInterval;
                if (adjustedWallaceBvsALower < 0.0)
                    adjustedWallaceBvsALower = 0.0;
                adjustedWallaceBvsAUpper = adjustedWallaceBvsA + adjustedWallaceBvsAConfInterval;
                if (adjustedWallaceBvsAUpper > 1.0)
                    adjustedWallaceBvsAUpper = 0.0;


                double wallaceAvsBConfInterval = 2.0 * Math.Sqrt(varWallaceAvsB);
                wallaceAvsBLower = wallaceAvsB - wallaceAvsBConfInterval;
                if (wallaceAvsBLower < 0.0)
                    wallaceAvsBLower = 0.0;
                wallaceAvsBUpper = wallaceAvsB + wallaceAvsBConfInterval;
                if (wallaceAvsBUpper > 1.0)
                    wallaceAvsBUpper = 1.0;

                double wallaceBvsAConfInterval = 2.0 * Math.Sqrt(varWallaceAvsB);
                wallaceBvsALower = wallaceBvsA - wallaceBvsAConfInterval;
                if (wallaceBvsALower < 0.0)
                    wallaceBvsALower = 0.0;
                wallaceBvsAUpper = wallaceBvsA + wallaceBvsAConfInterval;
                if (wallaceBvsAUpper > 1.0)
                    wallaceBvsAUpper = 1.0;
            }
            else
            {
                wallaceIA = 1.0 - simpsonsA;
                wallaceIB = 1.0 - simpsonsB;
                if (Math.Abs(simpsonsA - 1.0) < double.Epsilon)
                {
                    wallaceAvsB = 1.0;
                    wallaceAvsBLower = 1.0;
                    wallaceAvsBUpper = 1.0;
                    wallaceBvsA = 0.0;
                    wallaceBvsALower = 0.0;
                    wallaceBvsAUpper = 0.0;
                    adjustedWallaceAvsB = 1.0;
                    adjustedWallaceAvsBUpper = 1.0;
                    adjustedWallaceAvsBLower = 1.0;
                    adjustedWallaceBvsA = 0.0;
                    adjustedWallaceBvsAUpper = 0.0;
                    adjustedWallaceBvsALower = 0.0;
                }
                else
                {
                    wallaceAvsB = 0.0;
                    wallaceAvsBLower = 0.0;
                    wallaceAvsBUpper = 0.0;
                    wallaceBvsA = 1.0;
                    wallaceBvsALower = 1.0;
                    wallaceBvsAUpper = 1.0;
                    adjustedWallaceAvsB = 0.0;
                    adjustedWallaceAvsBUpper = 0.0;
                    adjustedWallaceAvsBLower = 0.0;
                    adjustedWallaceBvsA = 1.0;
                    adjustedWallaceBvsAUpper = 1.0;
                    adjustedWallaceBvsALower = 1.0;
                }
            }
        }

        private static Dictionary<string, Dictionary<string, int>> ContTable(List<string> ar1, List<string> ar2)
        {
            var cont = new Dictionary<string, Dictionary<string, int>>();

            for (int i = 0; i < ar1.Count; i++)
            {
                string keyArray1 = ar1[i];
                string keyArray2 = ar2[i];
                if (cont.ContainsKey(keyArray1))
                {
                    if (cont[keyArray1].ContainsKey(keyArray2))
                    {
                        cont[keyArray1][keyArray2]++;
                    }
                    else
                    {
                        cont[keyArray1].Add(keyArray2, 1);
                    }
                }
                else
                {
                    cont.Add(keyArray1, new Dictionary<string, int> { { keyArray2, 1 } });
                }
            }
            return cont;
        }

        private static void GetContTableHeaders(Dictionary<string, Dictionary<string, int>> cont,
                                                ref List<string> headers1,
                                                ref List<string> headers2)
        {
            var uniqueHeaders2 = new HashSet<string>();
            foreach (string key1 in cont.Keys)
            {
                headers1.Add(key1);
                foreach (string key2 in cont[key1].Keys)
                {
                    uniqueHeaders2.Add(key2);
                }
            }
            headers2.AddRange(uniqueHeaders2);
        }

        private static void GetContTableTotals(Dictionary<string, Dictionary<string, int>> cont,
                                               List<string> headers1,
                                               List<string> headers2,
                                               ref Dictionary<string, int> sumCol,
                                               ref Dictionary<string, int> sumRow,
                                               out int total)
        {
            foreach (string header2 in headers2)
            {
                sumRow.Add(header2, 0);
                foreach (string header1 in headers1)
                {
                    if (!cont[header1].ContainsKey(header2)) continue;
                    int val = cont[header1][header2];
                    sumRow[header2] += val;
                    if (sumCol.ContainsKey(header1))
                    {
                        sumCol[header1] += val;
                    }
                    else
                    {
                        sumCol.Add(header1, val);
                    }
                }
            }
            total = 0;
            foreach (string header1 in headers1)
            {
                total += sumCol[header1];
            }
        }

        private static void GetMisMatchMatrix(Dictionary<string, Dictionary<string, int>> cont,
                                              Dictionary<string, int> sumCol, Dictionary<string, int> sumRow, int n,
                                              out double a, out double b, out double c, out double d)
        {
            a = 0.0;
            foreach (var pair1 in cont)
            {
                foreach (var pair2 in pair1.Value)
                {
                    int val = pair2.Value;
                    a += (val * (val - 1d)) / 2d;
                }
            }
            double a1 = 0.0;
            foreach (int val in sumCol.Values)
            {
                a1 += (val * (val - 1d)) / 2d;
            }
            b = a1 - a;

            double a2 = 0.0;
            foreach (int val in sumRow.Values)
            {
                a2 += (val * (val - 1d)) / 2d;
            }
            c = a2 - a;

            d = ((n * (n - 1d)) / 2d) - a1 - c;
        }

        private static double GetRand(double a, double b, double c, double d)
        {
            return (a + d) / (a + b + c + d);
        }

        private static double GetAdjustedRand(double a, double b, double c, double d, double n)
        {
            return (((n * (n - 1)) / 2d) * (a + d) - ((a + b) * (a + c) + (c + d) * (b + d))) /
                   (Math.Pow((n * (n - 1)) / 2d, 2d) - ((a + b) * (a + c) + (c + d) * (b + d)));
        }

        private static void GetWallace(double a, double b, double c, out double wallaceAvsB, out double wallaceBvsA)
        {
            wallaceAvsB = (a + b > 0.0) ? a/(a + b) : 0.0;
            wallaceBvsA = (a + c > 0.0) ? a/(a + c) : 0.0;
        }

        private static double GetVarSimpsons(double cSum, double fc2, double fc3)
        {
            return (4.0 * cSum * (cSum - 1.0) * (cSum - 2.0) * fc3 + 2.0 * cSum * (cSum - 1.0) * fc2 -
                    2.0 * cSum * (cSum - 1.0) * (2.0 * cSum - 3.0) * (fc2 * fc2)) / ((cSum * (cSum - 1.0)) * (cSum * (cSum - 1.0)));
        }


        public static void GetSimpsons(List<string> ar,
                                       out double sid,
                                       out double sidLow,
                                       out double sidHigh,
                                       out int sidGroups)
        {
            int n = ar.Count;
            var dict = new Dictionary<string, int>();
            foreach (string s in ar)
            {
                if (dict.ContainsKey(s))
                {
                    dict[s]++;
                }
                else
                {
                    dict.Add(s, 1);
                }
            }

            int sumTotal = 0;
            double sumFc2 = 0;
            double sumFc3 = 0;

            foreach (int i in dict.Values)
            {
                sumTotal += (i * (i - 1));
                sumFc2 += Math.Pow(i / (double)n, 2d);
                sumFc3 += Math.Pow(i / (double)n, 3d);
            }

            if ((n * (n - 1)) > 0)
            {
                sid = 1d - (sumTotal / (double)(n * (n - 1)));
            }
            else
            {
                sid = 1;
            }

            double sqSumFc2 = Math.Pow(sumFc2, 2d);
            double s2 = (4 / (double)n) * (sumFc3 - sqSumFc2);

            sidLow = sid - 2 * Math.Sqrt(s2);
            sidHigh = sid + 2 * Math.Sqrt(s2);

            sidGroups = dict.Count;
        }
    }
}