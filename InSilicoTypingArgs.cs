using System;

namespace MicrobialInSilicoTyper
{
    public class InSilicoTypingArgs
    {
        private readonly Tuple<string, Marker, Progress> _strainTest;
        private readonly Exception _ex;
        private readonly string _message;

        public InSilicoTypingArgs(string message)
        {
            _message = message;
        }

        public InSilicoTypingArgs(Exception ex)
        {
            _ex = ex;
        }

        public InSilicoTypingArgs(Tuple<string, Marker, Progress> strainTest)
        {
            _strainTest = strainTest;
        }

        public Tuple<string, Marker, Progress> StrainTest
        {
            get { return _strainTest; }
        }

        public string Message
        {
            get { return _message; }
        }

        public Exception Exception
        {
            get { return _ex; }
        }

    }
}