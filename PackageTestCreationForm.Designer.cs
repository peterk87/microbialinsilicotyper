﻿namespace MicrobialInSilicoTyper
{
    partial class PackageTestCreationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPCR = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPCR = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPCRBrowse = new System.Windows.Forms.Button();
            this.tabAllele = new System.Windows.Forms.TabPage();
            this.gbxAlleleMarkerPreview = new System.Windows.Forms.GroupBox();
            this.lvwAllele = new MicrobialInSilicoTyper.ListViewFf();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtAlleleMarker = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAllelePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAlleleBrowse = new System.Windows.Forms.Button();
            this.btnAlleleAddMarker = new System.Windows.Forms.Button();
            this.tabProbe = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProbeIdentity = new System.Windows.Forms.TextBox();
            this.txtProbe = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnProbeBrowse = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.txtExtraInfo = new System.Windows.Forms.TextBox();
            this.lblExtraInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTestName = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExtraInfo = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            //
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPCR.SuspendLayout();
            this.tabAllele.SuspendLayout();
            this.gbxAlleleMarkerPreview.SuspendLayout();
            this.tabProbe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(465, 566);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(465, 566);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            this.toolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnRemove);
            this.splitContainer1.Panel2.Controls.Add(this.txtExtraInfo);
            this.splitContainer1.Panel2.Controls.Add(this.lblExtraInfo);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.txtTestName);
            this.splitContainer1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainer1.Panel2.Controls.Add(this.btnExtraInfo);
            this.splitContainer1.Panel2.Controls.Add(this.btnOK);
            this.splitContainer1.Panel2.Controls.Add(this.dgv);
            this.splitContainer1.Size = new System.Drawing.Size(465, 566);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPCR);
            this.tabControl1.Controls.Add(this.tabAllele);
            this.tabControl1.Controls.Add(this.tabProbe);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(461, 262);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPCR
            // 
            this.tabPCR.Controls.Add(this.label3);
            this.tabPCR.Controls.Add(this.txtPCR);
            this.tabPCR.Controls.Add(this.label1);
            this.tabPCR.Controls.Add(this.btnPCRBrowse);
            this.tabPCR.Location = new System.Drawing.Point(4, 22);
            this.tabPCR.Name = "tabPCR";
            this.tabPCR.Padding = new System.Windows.Forms.Padding(3);
            this.tabPCR.Size = new System.Drawing.Size(453, 236);
            this.tabPCR.TabIndex = 0;
            this.tabPCR.Text = "PCR";
            this.tabPCR.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(-1, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(473, 32);
            this.label3.TabIndex = 7;
            this.label3.Text = "Tab-delimited file with 5 rows needed - marker name, forward primer sequence, rev" +
    "erse primer sequence, amplicon size, amplicon acceptable range facter (decimal v" +
    "alue) - in that order";
            // 
            // txtPCR
            // 
            this.txtPCR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPCR.Location = new System.Drawing.Point(3, 71);
            this.txtPCR.Multiline = true;
            this.txtPCR.Name = "txtPCR";
            this.txtPCR.Size = new System.Drawing.Size(447, 162);
            this.txtPCR.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Paste PCR primer table into textbox or select file";
            // 
            // btnPCRBrowse
            // 
            this.btnPCRBrowse.Location = new System.Drawing.Point(237, 42);
            this.btnPCRBrowse.Name = "btnPCRBrowse";
            this.btnPCRBrowse.Size = new System.Drawing.Size(26, 23);
            this.btnPCRBrowse.TabIndex = 1;
            this.btnPCRBrowse.Text = "...";
            this.btnPCRBrowse.UseVisualStyleBackColor = true;
            // 
            // tabAllele
            // 
            this.tabAllele.Controls.Add(this.gbxAlleleMarkerPreview);
            this.tabAllele.Controls.Add(this.txtAlleleMarker);
            this.tabAllele.Controls.Add(this.label6);
            this.tabAllele.Controls.Add(this.label5);
            this.tabAllele.Controls.Add(this.txtAllelePath);
            this.tabAllele.Controls.Add(this.label4);
            this.tabAllele.Controls.Add(this.btnAlleleBrowse);
            this.tabAllele.Controls.Add(this.btnAlleleAddMarker);
            this.tabAllele.Location = new System.Drawing.Point(4, 22);
            this.tabAllele.Name = "tabAllele";
            this.tabAllele.Padding = new System.Windows.Forms.Padding(3);
            this.tabAllele.Size = new System.Drawing.Size(453, 236);
            this.tabAllele.TabIndex = 1;
            this.tabAllele.Text = "Allele Marker";
            this.tabAllele.UseVisualStyleBackColor = true;
            // 
            // gbxAlleleMarkerPreview
            // 
            this.gbxAlleleMarkerPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxAlleleMarkerPreview.Controls.Add(this.lvwAllele);
            this.gbxAlleleMarkerPreview.Location = new System.Drawing.Point(6, 76);
            this.gbxAlleleMarkerPreview.Name = "gbxAlleleMarkerPreview";
            this.gbxAlleleMarkerPreview.Size = new System.Drawing.Size(438, 125);
            this.gbxAlleleMarkerPreview.TabIndex = 7;
            this.gbxAlleleMarkerPreview.TabStop = false;
            this.gbxAlleleMarkerPreview.Text = "Allele marker Preview";
            // 
            // lvwAllele
            // 
            this.lvwAllele.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvwAllele.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwAllele.FullRowSelect = true;
            this.lvwAllele.GridLines = true;
            this.lvwAllele.Location = new System.Drawing.Point(3, 16);
            this.lvwAllele.Name = "lvwAllele";
            this.lvwAllele.Size = new System.Drawing.Size(432, 106);
            this.lvwAllele.TabIndex = 0;
            this.lvwAllele.UseCompatibleStateImageBehavior = false;
            this.lvwAllele.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Test Name";
            this.columnHeader1.Width = 92;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "marker Name";
            this.columnHeader2.Width = 107;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Allele Filename";
            this.columnHeader3.Width = 99;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Allele File Path";
            this.columnHeader4.Width = 127;
            // 
            // txtAlleleMarker
            // 
            this.txtAlleleMarker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAlleleMarker.Location = new System.Drawing.Point(80, 23);
            this.txtAlleleMarker.Name = "txtAlleleMarker";
            this.txtAlleleMarker.Size = new System.Drawing.Size(362, 20);
            this.txtAlleleMarker.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(395, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Specify the file path for the allele multifasta file (and specify a name for the " +
    "marker).";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "marker Name";
            // 
            // txtAllelePath
            // 
            this.txtAllelePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAllelePath.Location = new System.Drawing.Point(113, 49);
            this.txtAllelePath.Name = "txtAllelePath";
            this.txtAllelePath.Size = new System.Drawing.Size(297, 20);
            this.txtAllelePath.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Select allele file path";
            // 
            // btnAlleleBrowse
            // 
            this.btnAlleleBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlleleBrowse.Location = new System.Drawing.Point(416, 47);
            this.btnAlleleBrowse.Name = "btnAlleleBrowse";
            this.btnAlleleBrowse.Size = new System.Drawing.Size(28, 23);
            this.btnAlleleBrowse.TabIndex = 1;
            this.btnAlleleBrowse.Text = "...";
            this.btnAlleleBrowse.UseVisualStyleBackColor = true;
            // 
            // btnAlleleAddMarker
            // 
            this.btnAlleleAddMarker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlleleAddMarker.Enabled = false;
            this.btnAlleleAddMarker.Location = new System.Drawing.Point(288, 207);
            this.btnAlleleAddMarker.Name = "btnAlleleAddMarker";
            this.btnAlleleAddMarker.Size = new System.Drawing.Size(156, 23);
            this.btnAlleleAddMarker.TabIndex = 0;
            this.btnAlleleAddMarker.Text = "Add marker(s) To Test";
            this.btnAlleleAddMarker.UseVisualStyleBackColor = true;
            // 
            // tabProbe
            // 
            this.tabProbe.Controls.Add(this.label7);
            this.tabProbe.Controls.Add(this.txtProbeIdentity);
            this.tabProbe.Controls.Add(this.txtProbe);
            this.tabProbe.Controls.Add(this.label8);
            this.tabProbe.Controls.Add(this.btnProbeBrowse);
            this.tabProbe.Location = new System.Drawing.Point(4, 22);
            this.tabProbe.Name = "tabProbe";
            this.tabProbe.Padding = new System.Windows.Forms.Padding(3);
            this.tabProbe.Size = new System.Drawing.Size(453, 236);
            this.tabProbe.TabIndex = 2;
            this.tabProbe.Text = "Probe Hybridization";
            this.tabProbe.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(321, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Min. % Identity";
            // 
            // txtProbeIdentity
            // 
            this.txtProbeIdentity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProbeIdentity.Location = new System.Drawing.Point(402, 5);
            this.txtProbeIdentity.Name = "txtProbeIdentity";
            this.txtProbeIdentity.Size = new System.Drawing.Size(45, 20);
            this.txtProbeIdentity.TabIndex = 11;
            this.txtProbeIdentity.Text = "90";
            this.txtProbeIdentity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtProbe
            // 
            this.txtProbe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProbe.Location = new System.Drawing.Point(4, 32);
            this.txtProbe.Multiline = true;
            this.txtProbe.Name = "txtProbe";
            this.txtProbe.Size = new System.Drawing.Size(447, 201);
            this.txtProbe.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(227, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Paste probe multifasta into textbox or select file";
            // 
            // btnProbeBrowse
            // 
            this.btnProbeBrowse.Location = new System.Drawing.Point(235, 3);
            this.btnProbeBrowse.Name = "btnProbeBrowse";
            this.btnProbeBrowse.Size = new System.Drawing.Size(26, 23);
            this.btnProbeBrowse.TabIndex = 8;
            this.btnProbeBrowse.Text = "...";
            this.btnProbeBrowse.UseVisualStyleBackColor = true;
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.Location = new System.Drawing.Point(379, 174);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 11;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            // 
            // txtExtraInfo
            // 
            this.txtExtraInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtraInfo.Location = new System.Drawing.Point(156, 239);
            this.txtExtraInfo.Name = "txtExtraInfo";
            this.txtExtraInfo.Size = new System.Drawing.Size(266, 20);
            this.txtExtraInfo.TabIndex = 10;
            // 
            // lblExtraInfo
            // 
            this.lblExtraInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblExtraInfo.AutoSize = true;
            this.lblExtraInfo.Location = new System.Drawing.Point(3, 242);
            this.lblExtraInfo.Name = "lblExtraInfo";
            this.lblExtraInfo.Size = new System.Drawing.Size(147, 13);
            this.lblExtraInfo.TabIndex = 9;
            this.lblExtraInfo.Text = "Extra information file (optional)";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Test Name";
            // 
            // txtTestName
            // 
            this.txtTestName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTestName.Location = new System.Drawing.Point(68, 203);
            this.txtTestName.Name = "txtTestName";
            this.txtTestName.Size = new System.Drawing.Size(386, 20);
            this.txtTestName.TabIndex = 3;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(298, 266);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnExtraInfo
            // 
            this.btnExtraInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExtraInfo.Location = new System.Drawing.Point(428, 237);
            this.btnExtraInfo.Name = "btnExtraInfo";
            this.btnExtraInfo.Size = new System.Drawing.Size(26, 23);
            this.btnExtraInfo.TabIndex = 8;
            this.btnExtraInfo.Text = "...";
            this.btnExtraInfo.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(379, 266);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // dgv
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Blue;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(3, 6);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(451, 162);
            this.dgv.TabIndex = 5;
            // 
            // FrmPackageTestCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 566);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "PackageTestCreationForm";
            this.Text = "Create/Edit In Silico Typing Test";
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPCR.ResumeLayout(false);
            this.tabPCR.PerformLayout();
            this.tabAllele.ResumeLayout(false);
            this.tabAllele.PerformLayout();
            this.gbxAlleleMarkerPreview.ResumeLayout(false);
            this.tabProbe.ResumeLayout(false);
            this.tabProbe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPCR;
        private System.Windows.Forms.TabPage tabAllele;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPCRBrowse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTestName;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.TextBox txtPCR;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtExtraInfo;
        private System.Windows.Forms.Label lblExtraInfo;
        private System.Windows.Forms.Button btnExtraInfo;
        private System.Windows.Forms.GroupBox gbxAlleleMarkerPreview;
        private ListViewFf lvwAllele;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TextBox txtAlleleMarker;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAllelePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAlleleBrowse;
        private System.Windows.Forms.Button btnAlleleAddMarker;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.TabPage tabProbe;
        private System.Windows.Forms.TextBox txtProbe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnProbeBrowse;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtProbeIdentity;
    }
}