﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Enumerable = System.Linq.Enumerable;

namespace MicrobialInSilicoTyper
{
    /// <summary></summary>
    [Serializable]
    public class ExtraTestInfo
    {
        private readonly List<Marker> _markers;

        private readonly FileInfo _fileInfo;
        public FileInfo FileInfo { get { return _fileInfo; } }
        public string FilePath { get { return _fileInfo.FullName; } }

        private readonly string _testName;
        public string TestName { get { return _testName; } }

        private readonly Dictionary<Marker,int> _markerIndices = new Dictionary<Marker, int>();

        readonly Dictionary<int, Marker> _indexMarkerDict = new Dictionary<int, Marker>(); 

        private readonly List<string[]> _lines = new List<string[]>();


        readonly Dictionary<string, Dictionary<string, HashSet<int>>> _markerValueDict  = new Dictionary<string, Dictionary<string, HashSet<int>>>();

        readonly Dictionary<string, int> _fieldIndexDict = new Dictionary<string, int>();
 
        private string[] _fields;
        public string[] Fields { get { return _fields; } }

        public List<string> GetExtraInfoHeaders()
        {
            var tmp = new List<string>();
            for (int i = 0; i < _fields.Length; i++)
            {
                if (!_indexMarkerDict.ContainsKey(i))
                {
                    tmp.Add(_fields[i]);
                }
            }
            return tmp;
        }

        /// <summary>Provided a list of Marker, test name and file path to read the extra info from the extra information can be parsed into the output of the program</summary>
        /// <param name="markers">List of all Marker in the in silico typing test.</param>
        /// <param name="fileInfo">Extra information file information object.</param>
        /// <param name="testName">Test name corresponding to the extra information file and Marker in the Marker list.</param>
        public ExtraTestInfo(List<Marker> markers, FileInfo fileInfo, string testName)
        {
            _markers = markers;
            _fileInfo = fileInfo;
            _testName = testName;
        }

        public bool Read()
        {
            try
            {
                using (var sr = new StreamReader(FilePath))
                {
                    var readLine = sr.ReadLine();
                    if (readLine != null) _fields = readLine.Split('\t');


                    var fieldCount = 0;
                    foreach (string field in _fields)
                    {
                        if (_fieldIndexDict.ContainsKey(field)) continue;
                        _fieldIndexDict.Add(field, fieldCount);
                        fieldCount++;
                        if (!_markerValueDict.ContainsKey(field))
                            _markerValueDict.Add(field, new Dictionary<string, HashSet<int>>());
                    }

                    foreach (Marker marker in _markers)
                    {
                        if (marker.TestName != _testName) continue;
                        int markerIndex;
                        if (!_fieldIndexDict.TryGetValue(marker.Name, out markerIndex)) continue;
                        _markerIndices.Add(marker, markerIndex);
                        _indexMarkerDict.Add(markerIndex, marker);
                    }
                    int lineCount = 0;
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine();
                        if (line == null) 
                            continue;
                        string[] tmp = line.Split('\t');
                        if (tmp.Length != _fields.Length)
                            throw new Exception();
                        _lines.Add(tmp);
                        for (int i = 0; i < tmp.Length; i++)
                        {
                            string field = _fields[i];
                            Dictionary<string, HashSet<int>> fieldValueDict;
                            if (!_markerValueDict.TryGetValue(field, out fieldValueDict)) continue;
                            string fieldValue = tmp[i];
                            if (fieldValueDict.ContainsKey(fieldValue))
                            {
                                fieldValueDict[fieldValue].Add(lineCount);
                            }
                            else
                            {
                                fieldValueDict.Add(fieldValue, new HashSet<int>{lineCount});
                            }
                        }
                        lineCount++;
                    }
                }


            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>Get only the matching extra information; none of the marker information.</summary>
        /// <param name="markerMatches">List of matches to look for matching extra information with.</param>
        /// <param name="fuzzyMatching"> </param>
        public List<string> GetExtraInfo(IEnumerable<MarkerMatch> markerMatches, bool fuzzyMatching)
        {
            var extraInfoIndices = new HashSet<int>();
            for (int i = 0; i < _fields.Length; i++)
            {
                if (!_indexMarkerDict.ContainsKey(i))
                {
                    extraInfoIndices.Add(i);
                }
            }
            var hash = new HashSet<int>();
            for (int i = 0; i < _lines.Count; i++)
            {
                hash.Add(i);
            }
            foreach (MarkerMatch match in markerMatches)
            {
                var marker = match.Marker;

                Dictionary<string, HashSet<int>> markerValues;
                if (!_markerValueDict.TryGetValue(marker.Name, out markerValues)) continue;
                string result;
                if (match.Marker.TypingTest == TestType.Allelic)
                {
                    result = fuzzyMatching ? 
                        Misc.NumberRegex.Match(match.AlleleMatch).Value :
                        match.MarkerCall;
                }
                else
                {
                    result = match.MarkerCall;
                }
                    

                HashSet<int> resultIndices;
                HashSet<int> nullResultIndices;
                if (markerValues.TryGetValue(result, out resultIndices))
                {
                        
                    if (markerValues.TryGetValue("", out nullResultIndices))
                    {
                        resultIndices.UnionWith(nullResultIndices);
                    }
                    hash.IntersectWith(resultIndices);
                }
                else
                {
                    if (markerValues.TryGetValue("", out nullResultIndices))
                    {
                        hash.IntersectWith(nullResultIndices);
                    }
                    else
                    {
                        hash.Clear();
                        return new List<string>();
                    }
                }
            }

             var list = extraInfoIndices
                 .Select(extraInfoIndex => hash
                    .Select(i => _lines[i][extraInfoIndex])
                    .Distinct()
                    .ToList())
                 .Select(extraInfo =>
                             {
                                 extraInfo.Remove("");
                                 return string.Join(" ; ", extraInfo);
                             })
                 .ToList();

            return list;
        }

    }
}
