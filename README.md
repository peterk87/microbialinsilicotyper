# MIST: a tool for rapid *in silico* generation of molecular data from bacterial genome sequences


## Abstract 
  Whole-genome sequence (WGS) data can, in principle, resolve bacterial isolates that differ by a single base pair, thus providing the highest level of discriminatory power for epidemiologic subtyping.
  Nonetheless, because the capability to perform whole-genome sequencing in the context of epidemiological investigations involving priority pathogens has only recently become practical, fewer isolates have WGS data available relative to traditional subtyping methods. 
  It will be important to link these WGS data to data in traditional typing databases such as PulseNet and PubMLST in order to place them into proper historical and epidemiological context, thus enhancing investigative capabilities in response to public health events. 
  We present MIST (Microbial *In Silico* Typer), a bioinformatics tool for rapidly generating *in silico* typing data (e.g. MLST, MLVA) from draft bacterial genome assemblies. 
  MIST is highly customizable, allowing the analysis of existing typing methods along with novel typing schemes. 
  Rapid *in silico* typing provides a link between historical typing data and WGS data, while also providing a framework for the assessment of molecular typing methods based on WGS analysis.



## Paper

The full paper is available [here (pdf)](https://bitbucket.org/peterk87/microbialinsilicotyper/wiki/mist_paper.pdf).


## Citation

Cite as:

**MIST: a tool for rapid in silico generation of molecular data from bacterial genome sequences**.
Kruczkiewicz, Peter and Mutschall, Steven and Barker, Dillon and Thomas, James and Van Domselaar, Gary and Gannon, Victor P.J. and Carrillo, Catherine D. and Taboada, Eduardo N.
*Proceedings of Bioinformatics 2013: 4th International Conference on Bioinformatics Models, Methods and Algorithms.*
**2013**.
316–323.


Bibtex citation:

```
@article{kruczkiewicz_mist:_2013,
	title = {{MIST:} a tool for rapid in silico generation of molecular data from bacterial genome sequences},
	lccn = {0000},
	journal = {Proceedings of Bioinformatics 2013: 4th International Conference on Bioinformatics Models, Methods and Algorithms},
	author = {Kruczkiewicz, Peter and Mutschall, Steven and Barker, Dillon and Thomas, James and Van Domselaar, Gary and Gannon, Victor {P.J.} and Carrillo, Catherine D. and Taboada, Eduardo N.},
	year = {2013},
	pages = {316–323}
}
```




## Download

Download [MIST](https://bitbucket.org/peterk87/microbialinsilicotyper/wiki/MIST_GUI.7z) (GUI version, commit=dbfe7b4) with example analysis packages for *Campylobacter, E. coli, K. pneumoniae, L. monocytogenes*



## Command-line version

Command-line version of MIST available [here](https://bitbucket.org/peterk87/mist/overview)