﻿namespace MicrobialInSilicoTyper
{
    partial class MatchInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.olvMatchInfo = new BrightIdeasSoftware.FastObjectListView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.olvStrains = new BrightIdeasSoftware.FastObjectListView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.olvTests = new BrightIdeasSoftware.FastObjectListView();
            this.olvMarkers = new BrightIdeasSoftware.FastObjectListView();
            this.cmsMatchInfo = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvMatchInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvStrains)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvMarkers)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.olvMatchInfo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(635, 354);
            this.splitContainer1.SplitterDistance = 216;
            this.splitContainer1.TabIndex = 1;
            // 
            // olvMatchInfo
            // 
            this.olvMatchInfo.CheckBoxes = false;
            this.olvMatchInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvMatchInfo.IncludeColumnHeadersInCopy = true;
            this.olvMatchInfo.Location = new System.Drawing.Point(0, 0);
            this.olvMatchInfo.Name = "olvMatchInfo";
            this.olvMatchInfo.ShowGroups = false;
            this.olvMatchInfo.Size = new System.Drawing.Size(631, 212);
            this.olvMatchInfo.TabIndex = 0;
            this.olvMatchInfo.UseCompatibleStateImageBehavior = false;
            this.olvMatchInfo.View = System.Windows.Forms.View.Details;
            this.olvMatchInfo.VirtualMode = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.olvStrains);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(635, 134);
            this.splitContainer2.SplitterDistance = 191;
            this.splitContainer2.TabIndex = 0;
            // 
            // olvStrains
            // 
            this.olvStrains.CheckBoxes = false;
            this.olvStrains.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvStrains.Location = new System.Drawing.Point(0, 0);
            this.olvStrains.Name = "olvStrains";
            this.olvStrains.ShowGroups = false;
            this.olvStrains.Size = new System.Drawing.Size(187, 130);
            this.olvStrains.TabIndex = 1;
            this.olvStrains.UseCompatibleStateImageBehavior = false;
            this.olvStrains.View = System.Windows.Forms.View.Details;
            this.olvStrains.VirtualMode = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.olvTests);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.olvMarkers);
            this.splitContainer3.Size = new System.Drawing.Size(440, 134);
            this.splitContainer3.SplitterDistance = 223;
            this.splitContainer3.TabIndex = 0;
            // 
            // olvTests
            // 
            this.olvTests.CheckBoxes = false;
            this.olvTests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvTests.Location = new System.Drawing.Point(0, 0);
            this.olvTests.Name = "olvTests";
            this.olvTests.ShowGroups = false;
            this.olvTests.Size = new System.Drawing.Size(219, 130);
            this.olvTests.TabIndex = 1;
            this.olvTests.UseCompatibleStateImageBehavior = false;
            this.olvTests.View = System.Windows.Forms.View.Details;
            this.olvTests.VirtualMode = true;
            // 
            // olvMarkers
            // 
            this.olvMarkers.CheckBoxes = false;
            this.olvMarkers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvMarkers.Location = new System.Drawing.Point(0, 0);
            this.olvMarkers.Name = "olvMarkers";
            this.olvMarkers.ShowGroups = false;
            this.olvMarkers.Size = new System.Drawing.Size(209, 130);
            this.olvMarkers.TabIndex = 1;
            this.olvMarkers.UseCompatibleStateImageBehavior = false;
            this.olvMarkers.View = System.Windows.Forms.View.Details;
            this.olvMarkers.VirtualMode = true;
            // 
            // cmsMatchInfo
            // 
            this.cmsMatchInfo.Name = "cmsMatchInfo";
            this.cmsMatchInfo.Size = new System.Drawing.Size(61, 4);
            // 
            // MatchInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 354);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MatchInfoForm";
            this.Text = "In Silico Match Information";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvMatchInfo)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvStrains)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvMarkers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private BrightIdeasSoftware.FastObjectListView olvMatchInfo;
        private BrightIdeasSoftware.FastObjectListView olvStrains;
        private BrightIdeasSoftware.FastObjectListView olvTests;
        private BrightIdeasSoftware.FastObjectListView olvMarkers;
        private System.Windows.Forms.ContextMenuStrip cmsMatchInfo;

    }
}