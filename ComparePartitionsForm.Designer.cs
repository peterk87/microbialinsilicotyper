﻿namespace MicrobialInSilicoTyper
{
    partial class ComparePartitionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComparePartitionsForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.saveTypingDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePartitionCongruenceDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.olvMethods = new BrightIdeasSoftware.FastObjectListView();
            this.txtTypingData = new System.Windows.Forms.TextBox();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.olvMethodB = new BrightIdeasSoftware.FastObjectListView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.olvMethodA = new BrightIdeasSoftware.FastObjectListView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dlvPartitionCongruence = new BrightIdeasSoftware.FastDataListView();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvMethods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvMethodB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvMethodA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlvPartitionCongruence)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(928, 525);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(928, 550);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddbFile});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(50, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // ddbFile
            // 
            this.ddbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveTypingDataToolStripMenuItem,
            this.savePartitionCongruenceDataToolStripMenuItem});
            this.ddbFile.Image = ((System.Drawing.Image)(resources.GetObject("ddbFile.Image")));
            this.ddbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFile.Name = "ddbFile";
            this.ddbFile.Size = new System.Drawing.Size(38, 22);
            this.ddbFile.Text = "File";
            // 
            // saveTypingDataToolStripMenuItem
            // 
            this.saveTypingDataToolStripMenuItem.Name = "saveTypingDataToolStripMenuItem";
            this.saveTypingDataToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.saveTypingDataToolStripMenuItem.Text = "Save Typing Data";
            // 
            // savePartitionCongruenceDataToolStripMenuItem
            // 
            this.savePartitionCongruenceDataToolStripMenuItem.Name = "savePartitionCongruenceDataToolStripMenuItem";
            this.savePartitionCongruenceDataToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.savePartitionCongruenceDataToolStripMenuItem.Text = "Save Partition Congruence Data";
            // 
            // olvMethods
            // 
            this.olvMethods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvMethods.Location = new System.Drawing.Point(0, 0);
            this.olvMethods.Name = "olvMethods";
            this.olvMethods.ShowGroups = false;
            this.olvMethods.Size = new System.Drawing.Size(230, 196);
            this.olvMethods.TabIndex = 1;
            this.olvMethods.UseCompatibleStateImageBehavior = false;
            this.olvMethods.View = System.Windows.Forms.View.Details;
            this.olvMethods.VirtualMode = true;
            // 
            // txtTypingData
            // 
            this.txtTypingData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTypingData.Location = new System.Drawing.Point(3, 16);
            this.txtTypingData.Multiline = true;
            this.txtTypingData.Name = "txtTypingData";
            this.txtTypingData.Size = new System.Drawing.Size(688, 177);
            this.txtTypingData.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer4.Size = new System.Drawing.Size(694, 525);
            this.splitContainer4.SplitterDistance = 325;
            this.splitContainer4.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dlvPartitionCongruence);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(694, 325);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Partition congruence metrics";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTypingData);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(694, 196);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Typing data used in partition congruence analysis";
            // 
            // olvMethodB
            // 
            this.olvMethodB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvMethodB.Location = new System.Drawing.Point(0, 0);
            this.olvMethodB.Name = "olvMethodB";
            this.olvMethodB.ShowGroups = false;
            this.olvMethodB.Size = new System.Drawing.Size(230, 170);
            this.olvMethodB.TabIndex = 1;
            this.olvMethodB.UseCompatibleStateImageBehavior = false;
            this.olvMethodB.View = System.Windows.Forms.View.Details;
            this.olvMethodB.VirtualMode = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.olvMethodB);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.olvMethods);
            this.splitContainer3.Size = new System.Drawing.Size(230, 370);
            this.splitContainer3.SplitterDistance = 170;
            this.splitContainer3.TabIndex = 0;
            // 
            // olvMethodA
            // 
            this.olvMethodA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvMethodA.Location = new System.Drawing.Point(0, 0);
            this.olvMethodA.Name = "olvMethodA";
            this.olvMethodA.ShowGroups = false;
            this.olvMethodA.Size = new System.Drawing.Size(230, 151);
            this.olvMethodA.TabIndex = 0;
            this.olvMethodA.UseCompatibleStateImageBehavior = false;
            this.olvMethodA.View = System.Windows.Forms.View.Details;
            this.olvMethodA.VirtualMode = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer1.Size = new System.Drawing.Size(928, 525);
            this.splitContainer1.SplitterDistance = 230;
            this.splitContainer1.TabIndex = 2;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.olvMethodA);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(230, 525);
            this.splitContainer2.SplitterDistance = 151;
            this.splitContainer2.TabIndex = 0;
            // 
            // dlvPartitionCongruence
            // 
            this.dlvPartitionCongruence.DataSource = null;
            this.dlvPartitionCongruence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlvPartitionCongruence.Location = new System.Drawing.Point(3, 16);
            this.dlvPartitionCongruence.Name = "dlvPartitionCongruence";
            this.dlvPartitionCongruence.ShowGroups = false;
            this.dlvPartitionCongruence.Size = new System.Drawing.Size(688, 306);
            this.dlvPartitionCongruence.TabIndex = 0;
            this.dlvPartitionCongruence.UseCompatibleStateImageBehavior = false;
            this.dlvPartitionCongruence.View = System.Windows.Forms.View.Details;
            this.dlvPartitionCongruence.VirtualMode = true;
            // 
            // ComparePartitionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 550);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "ComparePartitionsForm";
            this.Text = "ComparePartitionsForm";
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvMethods)).EndInit();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvMethodB)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvMethodA)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlvPartitionCongruence)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private BrightIdeasSoftware.FastObjectListView olvMethodA;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private BrightIdeasSoftware.FastObjectListView olvMethodB;
        private BrightIdeasSoftware.FastObjectListView olvMethods;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTypingData;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton ddbFile;
        private System.Windows.Forms.ToolStripMenuItem saveTypingDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePartitionCongruenceDataToolStripMenuItem;
        private BrightIdeasSoftware.FastDataListView dlvPartitionCongruence;


    }
}