namespace MicrobialInSilicoTyper
{
    public class MarkerMatchingProgress
    {
        private readonly ContigCollection _contigCollection;
        private readonly Marker _marker;
        private readonly bool _completed;

        public MarkerMatchingProgress(ContigCollection contigCollection, Marker marker, bool completed)
        {
            _contigCollection = contigCollection;
            _marker = marker;
            _completed = completed;
        }

        public ContigCollection ContigCollection { get { return _contigCollection; } }
        public Marker Marker { get { return _marker; } }
        public bool Completed { get { return _completed; } }
    }
}