using System;
using System.Globalization;

namespace MicrobialInSilicoTyper
{
    [Serializable]
    public class BlastOutput
    {
        private int _alignmentLength;
        private double _bitScore;
        private int _gaps;
        private int _mismatches;

        /// <summary>Percent identity is in the form "90" == 90% == 0.9</summary>
        private double _percentIdentity;

        private string _queryAln;

        private int _queryEndIndex;
        private int _queryLength;
        private string _queryName;
        private int _queryStartIndex;
        private bool _reverseComplement;
        private string _subjAln;
        private int _subjectEndIndex;
        private string _subjectName;
        private int _subjectStartIndex;
        public int Mismatches { get { return _mismatches; } set { } }
        public string QueryAln { get { return _queryAln; } set { } }
        public string SubjAln { get { return _subjAln; } set { } }
        public int Gaps { get { return _gaps; } set { } }

        public string QueryName { get { return _queryName; } set { } }

        public string SubjectName { get { return _subjectName; } set { } }

        /// <summary>Percent identity is in the form "90.0" == 90.0% == 0.900</summary>
        public double PercentIdentity { get { return _percentIdentity; } set { } }

        public int SubjectStartIndex { get { return _subjectStartIndex; } set { } }
        public int SubjectEndIndex { get { return _subjectEndIndex; } set { } }
        public bool ReverseComplement { get { return _reverseComplement; } set { } }
        public int AlignmentLength { get { return _alignmentLength; } set { } }
        public double BitScore { get { return _bitScore; } set { } }
        public int QueryLength { get { return _queryLength; } set { } }

        public int QueryEndIndex { get { return _queryEndIndex; } set { } }
        public int QueryStartIndex { get { return _queryStartIndex; } set { } }

        /// <summary>If parsing of tab split string is successful no exception will be returned (null) else an exception will be returned.</summary>
        /// <param name="tmp">Tab split string containing a BLAST hit.</param>
        /// <returns></returns>
        public Exception ParseBlastResult(string[] tmp)
        {
            try
            {
                /*Format for BLAST output:
                 * 0 - query ID
                 * 1 - subj ID
                 * 2 - %ID
                 * 3 - length of hit
                 * 4 - query start
                 * 5 - query end
                 * 6 - subj start
                 * 7 - subj end
                 * 8 - query length
                 * 9 - bitscore
                */
                _queryName = tmp[0];
                _subjectName = tmp[1];
                _percentIdentity = double.Parse(tmp[2]);
                _alignmentLength = int.Parse(tmp[3]);
                _queryStartIndex = int.Parse(tmp[4]);
                _queryEndIndex = int.Parse(tmp[5]);
                _subjectStartIndex = int.Parse(tmp[6]);
                _subjectEndIndex = int.Parse(tmp[7]);
                _queryLength = int.Parse(tmp[8]);
                _bitScore = double.Parse(tmp[9]);
                _gaps = int.Parse(tmp[10]);
                _subjAln = tmp[11];
                _queryAln = tmp[12];
                _mismatches = int.Parse(tmp[13]);
                if (_subjectStartIndex > _subjectEndIndex)
                {
                    int i = _subjectEndIndex;
                    _subjectEndIndex = _subjectStartIndex;
                    _subjectStartIndex = i;
                    _reverseComplement = true;
                }
                else
                {
                    _reverseComplement = false;
                }
            }
            catch (Exception exception)
            {
                return exception;
            }
            return null;
        }
    }
}