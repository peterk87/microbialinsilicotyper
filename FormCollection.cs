﻿using System.Collections.Generic;

namespace MicrobialInSilicoTyper
{
    public class FormCollection
    {
        private readonly List<MatchInfoForm> _matchInfo = new List<MatchInfoForm>();

        private readonly MainForm _mainForm;
        public MainForm MainForm { get { return _mainForm; } }
        public ResultsForm ResultsFormWindow;
        

        public FormCollection(MainForm mainForm)
        {
            _mainForm = mainForm;
        }

        public void ProfileWindowClose()
        {
            if (ResultsFormWindow != null)
            {
                ResultsFormWindow.Close();
                ResultsFormWindow = null;
            }
        }

        public void CloseAllWindows()
        {
            foreach (MatchInfoForm f in _matchInfo)
            {
                f.Close();
            }
            _matchInfo.Clear();
            
            ProfileWindowClose();
        }
    }
}
