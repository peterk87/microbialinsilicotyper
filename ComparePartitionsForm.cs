﻿using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace MicrobialInSilicoTyper
{

    public partial class ComparePartitionsForm : Form
    {
        private readonly InSilicoTyping _ist;

        /// <summary>Typing results</summary>
        private readonly TypingResultsCollection _results;
        /// <summary>Data for partition congruence coefficient calculations.</summary>
        private Dictionary<string, List<string>> _cpData = new Dictionary<string, List<string>>();

        List<ComparePartitions> _cp = new List<ComparePartitions>(); 

        public ComparePartitionsForm(InSilicoTyping ist)
        {
            _ist = ist;
            InitializeComponent();

            _results = new TypingResultsCollection(_ist, true);

            _cpData = new Dictionary<string, List<string>>();
            _cp = new List<ComparePartitions>();
            GetComparePartitionsData();

            foreach (var p1 in _cpData)
            {
                string partition1 = p1.Key;
                var p1Data = p1.Value;
                foreach (var p2 in _cpData)
                {
                    string partition2 = p2.Key;
                    var p2Data = p2.Value;
                    if (partition1 == partition2)
                        continue;
                    _cp.Add(new ComparePartitions(p1Data, p2Data, partition1, partition2));
                }
            }
            var l = new List<Dictionary<string, object>>();
            foreach(var cp in _cp)
            {
                var dict = new Dictionary<string, object>
                    {
                        { "MethodA", cp.MethodA },
                        { "MethodB", cp.MethodB },
                        { "PartitionsA", cp.PartitionsA },
                        { "PartitionsB", cp.PartitionsB },
                        { "SimpsonsA", cp.SimpsonsA },
                        { "SimpsonsALower", cp.SimpsonsALower },
                        { "SimpsonsAUpper", cp.SimpsonsAUpper },
                        { "SimpsonsB", cp.SimpsonsB },
                        { "SimpsonsBUpper", cp.SimpsonsBUpper },
                        { "SimpsonsBLower", cp.SimpsonsBLower },
                        { "Rand", cp.Rand },
                        { "AdjustedRand", cp.AdjustedRand },
                        { "WallaceAvsB", cp.WallaceAvsB },
                        { "WallaceAvsBLower", cp.WallaceAvsBLower },
                        { "WallaceAvsBUpper", cp.WallaceAvsBUpper },
                        { "WallaceBvsA", cp.WallaceBvsA },
                        { "WallaceBvsALower", cp.WallaceBvsALower },
                        { "WallaceBvsAUpper", cp.WallaceBvsAUpper },
                        { "WallaceIa", cp.WallaceIa },
                        { "WallaceIb", cp.WallaceIb },
                        { "AdjustedWallaceAvsB", cp.AdjustedWallaceAvsB },
                        { "AdjustedWallaceAvsBUpper", cp.AdjustedWallaceAvsBUpper },
                        { "AdjustedWallaceAvsBLower", cp.AdjustedWallaceAvsBLower },
                        { "AdjustedWallaceBvsA", cp.AdjustedWallaceBvsA },
                        { "AdjustedWallaceBvsAUpper", cp.AdjustedWallaceBvsAUpper },
                        { "AdjustedWallaceBvsALower", cp.AdjustedWallaceBvsALower },
                    };
                l.Add(dict);
            }
            var dt = new DataTable();
            foreach (var p in l)
            {
                foreach (var key in p.Keys)
                {
                    dt.Columns.Add(key);
                }
                break;
            }
            foreach (var p in l)
            {
                DataRow dataRow = dt.NewRow();
                foreach (var p2 in p)
                {
                    string s = p2.Key;
                    dataRow[s] = p2.Value;
                }
                dt.Rows.Add(dataRow);
            }
            dlvPartitionCongruence.DataSource = dt;

        }

        private void GetComparePartitionsData()
        {
            //get all of the headers for each marker for each test including extra info each test
            foreach (var pair in _results.TestMarkerDict)
            {
                string testName = pair.Key;
                var testType = _results.TestTestTypesDict[testName];
                List<string> extraInfoHeaders = null;
                if (_results.TestExtraInfoDict[testName] != null)
                {
                    extraInfoHeaders = _results.TestExtraInfoDict[testName].GetExtraInfoHeaders();

                    foreach (var extraInfoHeader in extraInfoHeaders)
                    {
                        _cpData.Add(extraInfoHeader, new List<string>());
                    }
                }
                var markers = new List<Marker>(pair.Value);
                markers.Sort();



                _cpData.Add(testName, new List<string>());

                foreach (TypingResults typingResult in _results.Results)
                {
                    ContigCollection cc = typingResult.ContigCollection;
                    var markerMatches = new List<MarkerMatch>();

                    var markerMatchStrings = new List<string>();
                    foreach (Marker marker in markers)
                    {
                        //for the current marker, get the marker match data
                        MarkerMatch markerMatch = cc.MarkerMatchesDict[marker];
                        markerMatches.Add(markerMatch);
                        string matchData = GetMarkerMatchData(marker, markerMatch, false);

                        markerMatchStrings.Add(matchData);
                    }

                    _cpData[testName].Add(string.Join("-", markerMatchStrings));
                    //check if there is extra info for the current test
                    if (_results.TestExtraInfoDict[testName] == null)
                        continue;
                    //get the extra info
                    List<string> list = _results.TestExtraInfoDict[testName].GetExtraInfo(markerMatches, true);
                    int index = 0;
                    foreach (string extraInfo in list)
                    {
                        _cpData[extraInfoHeaders[index]].Add(extraInfo);
                        index++;
                    }

                }


            }
        }



        /// <summary>Get the marker match data (verbose or sparse) for a particular marker match.</summary>
        /// <param name="marker">marker.</param>
        /// <param name="mm">marker match.</param>
        /// <param name="verbose">Verbose or sparse data to be returned.</param>
        /// <returns>marker match data string.</returns>
        private static string GetMarkerMatchData(Marker marker, MarkerMatch mm, bool verbose)
        {
            switch (marker.TypingTest)
            {
                case TestType.AmpliconProbe:
                case TestType.OligoProbe:
                case TestType.PCR:
                    if (mm != null)
                    {
                        return mm.MarkerCall;
                    }
                    return ("0");
                case TestType.Allelic:
                    if (mm != null)
                    {
                        if (verbose)
                            return (mm.CorrectMarkerMatch
                                        ? mm.MarkerCall
                                        : mm.AlleleMatch + "; " + mm.Mismatches + " mismatches");
                        return mm.CorrectMarkerMatch ? mm.MarkerCall : "";
                    }
                    return "";
                case TestType.Repeat:
                    if (mm != null)
                    {
                        return mm.MarkerCall;
                    }
                    return "";
                case TestType.SNP:
                    if (mm != null)
                    {
                        return mm.MarkerCall;
                    }
                    return "";
            }
            return null;
        }

    }
}
