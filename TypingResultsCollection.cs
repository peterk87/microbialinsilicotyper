using System.Collections.Generic;
using System.Linq;

namespace MicrobialInSilicoTyper
{
    public class TypingResultsCollection
    {
        private readonly List<TypingResults> _results = new List<TypingResults>();

        private readonly InSilicoTyping _analysis;
        private readonly bool _showFuzzyMatching;

        private readonly Dictionary<string, TestType> _testTestTypesDict = new Dictionary<string, TestType>(); 
        private readonly Dictionary<string, HashSet<Marker>> _testMarkerDict = new Dictionary<string, HashSet<Marker>>();
        private readonly Dictionary<string, ExtraTestInfo> _testExtraInfoDict = new Dictionary<string, ExtraTestInfo>();
        private readonly Dictionary<string, HashSet<MarkerMatch>> _testMatchesDict = new Dictionary<string, HashSet<MarkerMatch>>();
        private List<ContigCollection> _analyzedGenomes = new List<ContigCollection>();
        public List<TypingResults> Results { get { return _results; } }
        public Dictionary<string, TestType> TestTestTypesDict { get { return _testTestTypesDict; } }
        public Dictionary<string, ExtraTestInfo> TestExtraInfoDict { get { return _testExtraInfoDict; } }
        public Dictionary<string, HashSet<Marker>> TestMarkerDict { get { return _testMarkerDict; } }

        public TypingResultsCollection(InSilicoTyping analysis, bool showFuzzyMatching)
        {
            _analysis = analysis;
            _analyzedGenomes = _analysis.MultifastaFileDict
                .Select(p => p.Value)
                .Where(cc => cc != null && cc.MarkerMatchesDict.Count == _analysis.Markers.Count(m => m.Selected))
                .ToList();
            
            
            if (_analyzedGenomes.Count == 0)
            {
                return;
            }
            _showFuzzyMatching = showFuzzyMatching;
            GetTestInformation();
            GetResults();
        }

        private void GetTestInformation()
        {

            foreach (var contigCollection in _analyzedGenomes)
            {
                foreach (MarkerMatch markerMatch in contigCollection.MarkerMatches)
                {
                    var testName = markerMatch.TestName;
                    if (_testTestTypesDict.ContainsKey(testName))
                    {
                        _testMarkerDict[testName].Add(markerMatch.Marker);
                        _testMatchesDict[testName].Add(markerMatch);
                    }
                    else
                    {
                        _testTestTypesDict.Add(testName, markerMatch.Marker.TypingTest);
                        _testMarkerDict.Add(testName, new HashSet<Marker> {markerMatch.Marker});
                        _testMatchesDict.Add(testName, new HashSet<MarkerMatch>{markerMatch});

                        ExtraTestInfo extra = null;
                        foreach (ExtraTestInfo extraInfo in _analysis.ExtraInfo)
                        {
                            if (extraInfo.TestName != testName) 
                                continue;
                            extra = extraInfo;
                            break;
                        }
                        _testExtraInfoDict.Add(testName, extra);
                    }
                }
            }
        }

        private void GetResults()
        {
            foreach (var cc in _analyzedGenomes)
            {
                if (cc == null)
                {
                    continue;
                }
                if (cc.MarkerMatches.Count == 0)
                {
                    continue;
                }
                _results.Add(new TypingResults(cc, this, _showFuzzyMatching));
            }
        }
    }
}