﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MicrobialInSilicoTyper
{
    public class PCRMarkerMatching
    {
        private readonly List<BlastOutput> _forwardPrimerMatches;
        private readonly Marker _marker;

        private readonly List<MarkerMatch> _markerMatches = new List<MarkerMatch>();
        private readonly List<BlastOutput> _reversePrimerMatches;

        private readonly Dictionary<string, List<BlastOutput>> _sortedForwardPrimerMatches;
        private readonly Dictionary<string, List<BlastOutput>> _sortedReversePrimerMatches;
        private readonly ContigCollection _subject;

        public PCRMarkerMatching(Marker marker, ContigCollection subject, DirectoryInfo workingDir, string subjectFilename)
        {
            _marker = marker;
            _subject = subject;

            if (Misc.IsDegenSequence(marker.ForwardPrimer))
            {
                var primers = new List<string>();
                Misc.ExpandDegenSequence(new char[marker.ForwardPrimer.Length], marker.ForwardPrimer, 0, primers);
                _forwardPrimerMatches = new List<BlastOutput>();
                foreach (string primer in primers)
                {
                    _forwardPrimerMatches.AddRange(GetBlastResults(marker.Name, primer, workingDir, subjectFilename));
                }
            }
            else
            {
                _forwardPrimerMatches = GetBlastResults(marker.Name, marker.ForwardPrimer, workingDir, subjectFilename);
            }

            if (Misc.IsDegenSequence(marker.ReversePrimer))
            {
                var primers = new List<string>();
                Misc.ExpandDegenSequence(new char[marker.ReversePrimer.Length], marker.ReversePrimer, 0, primers);
                _reversePrimerMatches = new List<BlastOutput>();
                foreach (string primer in primers)
                {
                    _reversePrimerMatches.AddRange(GetBlastResults(marker.Name, primer, workingDir, subjectFilename));
                }
            }
            else
            {
                _reversePrimerMatches = GetBlastResults(marker.Name, marker.ReversePrimer, workingDir, subjectFilename);
            }

            _sortedForwardPrimerMatches = SortBlastResults(_forwardPrimerMatches);
            _sortedReversePrimerMatches = SortBlastResults(_reversePrimerMatches);

            GetMarkerMatches();
        }

        public List<BlastOutput> ForwardPrimerMatches { get { return _forwardPrimerMatches; } }
        public List<BlastOutput> ReversePrimerMatches { get { return _reversePrimerMatches; } }
        public Dictionary<string, List<BlastOutput>> SortedForwardPrimerMatches { get { return _sortedForwardPrimerMatches; } }
        public Dictionary<string, List<BlastOutput>> SortedReversePrimerMatches { get { return _sortedReversePrimerMatches; } }
        public List<MarkerMatch> MarkerMatches { get { return _markerMatches; } }
        public ContigCollection Subject { get { return _subject; } }
        public Marker Marker { get { return _marker; } }


        private void GetMarkerMatches()
        {
            //find the pair of hits that generate the most likely amplicon
            foreach (var contigForwardMatches in _sortedForwardPrimerMatches)
            {
                List<BlastOutput> reverseMatches;
                if (!_sortedReversePrimerMatches.TryGetValue(contigForwardMatches.Key, out reverseMatches))
                    continue;
                List<BlastOutput> forwardMatches = contigForwardMatches.Value;

                Contig contig;
                if (!_subject.HeaderContigDict.TryGetValue(contigForwardMatches.Key, out contig))
                    continue;

                foreach (BlastOutput forwardMatch in forwardMatches)
                {
                    foreach (BlastOutput reverseMatch in reverseMatches)
                    {
                        MarkerMatch markerMatch = GetMarkerMatch(forwardMatch, reverseMatch, contig);
                        //only keep the marker matches that are correct (amplicon size is within the acceptable range)
                        if (markerMatch.CorrectMarkerMatch)
                            _markerMatches.Add(markerMatch);
                    }
                }
            }
        }

        private MarkerMatch GetMarkerMatch(BlastOutput forward, BlastOutput reverse, Contig contig)
        {
            return new MarkerMatch(forward, reverse, _marker, _subject, contig);
        }

        private static List<BlastOutput> GetBlastResults(string markerName, string markerPrimer, DirectoryInfo workingDir, string subjectFilename)
        {
            string query = string.Format(">{0}\n{1}", markerName, markerPrimer);
            var primerBlast = new BlastProcess(workingDir, query, subjectFilename, TestType.PCR);
            return primerBlast.BlastOutputs;
        }

        private Dictionary<string, List<BlastOutput>> SortBlastResults(List<BlastOutput> blastOutputs)
        {
            var dict = new Dictionary<string, List<BlastOutput>>();

            foreach (BlastOutput blastOutput in blastOutputs)
            {
                int subjectIndexID = Convert.ToInt32(blastOutput.SubjectName);
                string subjectID = _subject.Contigs[subjectIndexID].Header;
                if (dict.ContainsKey(subjectID))
                {
                    dict[subjectID].Add(blastOutput);
                }
                else
                {
                    dict.Add(subjectID, new List<BlastOutput> { blastOutput });
                }
            }
            return dict;
        }
    }
}