﻿using System.Windows.Forms;

namespace MicrobialInSilicoTyper
{
    public sealed class ListViewFf : ListView
    {
        
        public ListViewFf()
        {
            DoubleBuffered = true;
            View = View.Details;
            GridLines = true;
            Dock = DockStyle.Fill;
            FullRowSelect = true;
            //this.HideSelection = false;
        }

    }
}
