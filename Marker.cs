﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace MicrobialInSilicoTyper
{
    //Need the ability to detect and adjust analysis for degenerate primers
    [Serializable]
    public class Marker : IComparable<Marker>
    {
        private readonly string _allelicDatabaseFilename;
        private readonly double _ampliconRange = -1;
        private readonly int _ampliconSize;
        private readonly string _forwardPrimer;
        private readonly string _name;
        private readonly int _repeatSize;
        private readonly string _reversePrimer;
        private readonly string _testName;
        private readonly TestType _typingTest;

        public Marker(
            string name,
            string testName,
            TestType testType,
            string fprimer,
            string rprimer,
            int ampliconSize,
            double ampliconRange,
            string allelicDatabaseFilename,
            int repeatSize
            )
        {
            _typingTest = testType;
            _name = name;
            _testName = testName;
            _forwardPrimer = fprimer.ToUpper();
            _reversePrimer = rprimer.ToUpper();
            _ampliconSize = ampliconSize;
            _ampliconRange = ampliconRange;
            _allelicDatabaseFilename = allelicDatabaseFilename;
            _repeatSize = repeatSize;
            Selected = true;
        }

        public bool Selected { get; set; }

        public string TestName
        {
            get { return _testName; }
        }

        public TestType TypingTest
        {
            get { return _typingTest; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string ForwardPrimer
        {
            get { return _forwardPrimer; }
        }

        public string ReversePrimer
        {
            get { return _reversePrimer; }
        }

        public int AmpliconSize
        {
            get { return _ampliconSize; }
        }
        /// <summary>Amplicon range factor for PCR assays. BLAST %ID threshold for probe tests.</summary>
        public double AmpliconRange
        {
            get { return _ampliconRange; }
        }

        public int UpperAmpliconRange
        {
            get { return (int) Math.Ceiling(_ampliconSize*(1d + _ampliconRange)); }
        }

        public int LowerAmpliconRange
        {
            get { return (int) Math.Floor(_ampliconSize*(1d - _ampliconRange)); }
        }

        public string AllelicDatabaseFilename
        {
            get { return _allelicDatabaseFilename; }
        }

        public int RepeatSize
        {
            get { return _repeatSize; }
        }

        #region IComparable<marker> Members

        public int CompareTo(Marker other)
        {
            return String.CompareOrdinal(_name, other.Name);
        }

        #endregion

        public override string ToString()
        {
            var tmp = new List<string>
                          {
                              _name,
                              _testName,
                              ((int) _typingTest).ToString(CultureInfo.InvariantCulture),
                              _forwardPrimer,
                              _reversePrimer,
                              _ampliconSize.ToString(CultureInfo.InvariantCulture),
                              _ampliconRange.ToString(CultureInfo.InvariantCulture),
                              _allelicDatabaseFilename,
                              RepeatSize.ToString(CultureInfo.InvariantCulture)
                          };

            return string.Join("\t", tmp);
        }
    }
}